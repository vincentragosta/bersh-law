<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         '}2$GECQJpq)*8$l H/x2*Rn-fS]R%;|iTxUMl?D EZ}0.Pj$gDGSdM$--pR.f5E;');
define('SECURE_AUTH_KEY',  'Ej`--qY3HpvEmT2u-F1nBrH]#cV!MjQvZ>CY9$w!})vrA+@eI1bIC}.`j|emD0Sp');
define('LOGGED_IN_KEY',    '$3+dJ9*RStB/Y=)9)S|G{L L~Wi8wM[m#e+gdbr,CQ1L<$;{VSP9U?3XK4aRD([-');
define('NONCE_KEY',        'MGFqwC.crQ |p>Jnnc90oVtR|QrTBAL4OKj+N|V(i+: ]bg77}tV 0nvGTg-C:?I');
define('AUTH_SALT',        '!)^jbS#Esz?kk~Ba9F7DJ=TQA/^j&r=ii$=T&&xHIB70&$zgQHZmHto}?-E8VNG`');
define('SECURE_AUTH_SALT', ' 82]+PEmJMdsV/WP6dh$#)`UC`WrBBx8Q|-P?Xm/BKqc^u3kYz;UNtOe:<RN}2y<');
define('LOGGED_IN_SALT',   'TUV?!ClIDzGrh|3afTn1hp]MXqY]n?1pO{|S{?{ofa-;5NkP}Eeb;)x&}|)m[CCO');
define('NONCE_SALT',       'l:Mu?O6|f)>%^8WycBQWVqrw8+(3n]PTBojeJT}.DqnhW2IiL<n0P&TQpFKW|7Y`');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
