<?php
/*
This is a sample local-config.php file
In it, you *must* include the three main database defines

You may include other settings here for various environments
*/

define( 'DB_NAME', 'scotchbox' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root' );
