<?php
/**
 * Template Name: Construction Page
 */

use Backstage\Models\Page;

$Page = Page::createFromGlobal();
// trigger deployments

?>

<?php if (!empty($content = $Page->content(false))): ?>
    <?= $content; ?>
<?php endif; ?>
