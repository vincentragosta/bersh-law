<?php
/**
 * Expected:
 * @var string $text
 */

if (empty($text)) {
    return '';
}
?>

<div class="disclaimer">
    <?= $text; ?>
</div>
