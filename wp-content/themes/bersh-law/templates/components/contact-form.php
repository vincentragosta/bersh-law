<form method="post" id="contact-form" action="" class="custom-form">
    <div class="custom-form__row custom-form__row--inactive custom-form__errors">
        <h2 class="heading heading--default heading--inverted">There were errors with your submission:</h2>
        <ul></ul>
    </div>
    <div class="custom-form__row custom-form__row--inactive custom-form__progress-bar">
        <div class="custom-form__row-inner">
            <label for="custom-form__progress">Request Progress:</label>
            <progress id="custom-form__progress" class="custom-form__progress" value="0" max="100"></progress>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field">
                <label for="contact-form__full-name">Full Name <span>*</span></label>
                <input type="text" id="contact-form__full-name" name="contact-form__full-name" />
            </fieldset>
            <fieldset class="custom-form__field">
                <label for="contact-form__email">Email <span>*</span></label>
                <input type="email" id="contact-form__email" name="contact-form__email" />
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field">
                <label for="contact-form__phone">Phone <span>*</span></label>
                <input type="text" id="contact-form__phone" name="contact-form__phone" />
            </fieldset>
            <fieldset class="custom-form__field">
                <label for="contact-form__state">State <span>*</span></label>
                <select id="contact-form__state" name="contact-form__state">
                    <option value="">Select State</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field">
                <label for="contact-form__description">Please provide a brief summary of your legal matter <span>*</span></label>
                <textarea type="text" id="contact-form__description" name="contact-form__description"></textarea>
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field custom-form__field--checkbox">
                <label for="contact-form__disclaimer"><input id="contact-form__disclaimer" name="contact-form__disclaimer" type="checkbox"><p>I have read the <a href="#disclaimer">Disclaimer</a>. <span>*</span></p></label>
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <input type="submit" class="custom-form__submit button" value="Send Message">
        </div>
    </div>
</form>
