<?php
?>
<form method="post" id="inquiry-form" action="" class="custom-form" data-step-index="1" >
    <div class="custom-form__row custom-form__row--inactive custom-form__errors">
        <h2 class="heading heading--default heading--inverted">There were errors with your submission:</h2>
        <ul></ul>
    </div>
    <div class="custom-form__row" id="inquiry-form__step-display">
        <div class="custom-form__row-inner">
            <h2 class="heading heading--medium">Step <span>1</span></h2>
            <div>
                <p>This form is for real estate inquiries only. If your matter is not a real estate transaction, please fill out the form on the <a href="<?= home_url('/contact/'); ?>">Contact Us</a> page.</p>
                <p>This is a multi-step form, each of the fields must be filled in to submit the form, unless otherwise specified.</p>
                <p>Please delineate whether the client is an individual or a business entity. If the client is an estate, please select Business Entity as the Client Type. If there are multiple Executors of the estate, please fill out this form as though there are as many sellers as there are representatives.</p>
            </div>
        </div>
    </div>
    <div class="custom-form__row custom-form__row--inactive custom-form__progress-bar">
        <div class="custom-form__row-inner">
            <label for="custom-form__progress">Request Progress:</label>
            <progress id="custom-form__progress" class="custom-form__progress" value="0" max="100"></progress>
        </div>
    </div>
    <div class="custom-form__row" id="inquiry-form__steps">
        <div class="custom-form__row-inner" id="inquiry-form__step-1">
            <fieldset class="custom-form__field">
                <label for="inquiry-form__client-role-1">Client Role <span>*</span></label>
                <select id="inquiry-form__client-role-1" name="inquiry-form__client-role-1">
                    <option value="">Select Client Role</option>
                    <option value="buyer">Buyer</option>
                    <option value="seller">Seller</option>
                </select>
            </fieldset>
            <fieldset class="custom-form__field">
                <label for="inquiry-form__client-representation-1">Will Bershtein Law Represent this Client? <span>*</span></label>
                <select id="inquiry-form__client-representation-1" name="inquiry-form__client-representation-1">
                    <option value="">Select Representation</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
            </fieldset>
            <fieldset class="custom-form__field">
                <label for="inquiry-form__client-type-1">Client Type <span>*</span></label>
                <select id="inquiry-form__client-type-1" name="inquiry-form__client-type-1">
                    <option value="">Select Client Type</option>
                    <option value="individual">Individual</option>
                    <option value="business-entity">Business Entity</option>
                </select>
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row" id="inquiry-form__buttons">
        <div class="custom-form__row-inner">
            <button type="button" id="inquiry-form__previous-step" class="custom-form__button custom-form__button--inactive button" data-step-index="0">
                Previous Step: Client Type
            </button>
            <button type="button" id="inquiry-form__next-step" class="custom-form__button button">
                Next Step: Add Client Information
            </button>
            <input type="submit" class="custom-form__submit custom-form__submit--inactive button" value="Submit">
        </div>
    </div>
</form>
