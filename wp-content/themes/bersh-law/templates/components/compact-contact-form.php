<form method="post" id="compact-contact-form" action="" class="custom-form">
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field">
                <label for="compact-contact-form__email"></label>
                <input type="email" id="compact-contact-form__email" name="compact-contact-form__email" placeholder="Your Email Address*" required />
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field">
                <label for="compact-contact-form__description"></label>
                <textarea type="text" id="compact-contact-form__description" name="compact-contact-form__description" required>Describe your case briefly*</textarea>
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <fieldset class="custom-form__field custom-form__field--checkbox">
                <label for="compact-contact-form__disclaimer"><input id="compact-contact-form__disclaimer" name="compact-contact-form__disclaimer" type="checkbox" required><p>I have read the <a href="#disclaimer">Disclaimer</a>. <span>*</span></p></label>
            </fieldset>
        </div>
    </div>
    <div class="custom-form__row">
        <div class="custom-form__row-inner">
            <input type="submit" class="custom-form__submit button" value="Submit">
        </div>
    </div>
</form>
