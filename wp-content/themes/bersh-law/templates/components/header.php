<?php
use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Options\GlobalOptions;
?>
<header class="header-nav sticky-header" data-gtm="Header">
    <div class="header-nav__main">
        <div class="header-nav__container container">
            <?php if (($logo = GlobalOptions::logo()) instanceof WP_Image): ?>
                <div class="header-nav__logo-container">
                    <a href="<?= home_url() ?>">
                        <?= $logo->css_class('header-nav__logo') ?>
                    </a>
                </div>
            <?php endif; ?>
            <div class="header-nav__list-container">
                <ul class="header-nav__list list list--inline">
                    <?php if (!empty($phone = GlobalOptions::contactPhone())): ?>
                        <li class="header-nav__list-item header-nav__list-item--phone">
                            <a href="tel:<?= $phone; ?>"><?= new IconView(['icon_name' => 'phone', 'style' => 'primary']); ?> <span><?= $phone; ?></span></a>
                        </li>
                    <?php endif; ?>
                    <?php if (!empty($email = GlobalOptions::contactEmail())): ?>
                        <li class="header-nav__list-item header-nav__list-item--email">
                            <a href="mailto:<?= $email; ?>"><?= new IconView(['icon_name' => 'email', 'style' => 'primary']); ?> <span><?= $email; ?></span></a>
                        </li>
                    <?php endif; ?>
                    <li class="header-nav__list-item">
                        <hr class="vertical-line" />
                    </li>
                    <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                        <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                            <li class="header-nav__list-item">
                                <a href="<?= $SocialIcon->getUrl(); ?>" target="_blank" class="button--with-icon-vertical">
                                    <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                                    <small><?= $SocialIcon->getLabel(); ?></small>
                                </a>
                            </li>
                        <?php endforeach; ?>
                        <?php if (!empty($portal_url = GlobalOptions::clientPortalUrl())): ?>
                            <li class="header-nav__list-item">
                                <a href="<?= $portal_url; ?>" class="header-nav__portal-link" target="_blank"><?= new IconView(['icon_name' => 'portal']); ?> <span>Portal</span></a>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-nav__bottom">
        <div class="header-nav__container container">
            <?php if (has_nav_menu('primary_navigation')): ?>
                <div class="header-nav__menu">
                    <?= NavMenuView::createResponsive('primary_navigation'); ?>
                </div>
            <?php endif; ?>
            <div>
                <?php if (!empty($header_cta = GlobalOptions::headerCta()) && isset($header_cta['url'])): ?>
                    <a href="<?= $header_cta['url']; ?>" class="header-nav__cta button button--with-icon-horizontal" target="<?= $header_cta['target']; ?>">
                        <span><?= $header_cta['title']; ?></span>
                        <span><?= new IconView(['icon_name' => 'arrow']); ?></span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
