<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\Util;
use ChildTheme\Options\GlobalOptions;

if (!empty($disclaimer_text = GlobalOptions::disclaimerText())) {
    ModalView::load('disclaimer', 'box', Util::getTemplateScoped('templates/components/disclaimer.php', ['text' => $disclaimer_text]));
}
?>
<footer class="footer-nav">
    <div class="footer-nav__main">
        <div class="footer-nav__container container">
            <div class="footer-nav__row row">
                <div class="footer-nav__column col-12 col-sm-6 col-lg-4">
                    <div class="footer-nav__column-inner">
                        <?php if (($brand_image = GlobalOptions::logo()) instanceof WP_Image): ?>
                            <?= $brand_image->css_class('footer-nav__logo'); ?>
                        <?php endif; ?>
                        <div class="footer-nav__copyright">
                            <?php if (!empty($contact_address = GlobalOptions::contactAddress())): ?>
                                <p><?= $contact_address; ?></p>
                            <?php endif; ?>
                        </div>
                        <?php if (!empty($footer_text = GlobalOptions::footerText())): ?>
                            <div class="footer-nav__text">
                                <?= $footer_text; ?>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                            <ul class="footer-nav__list list list--inline">
                                <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                                    <li>
                                        <a href="<?= $SocialIcon->getUrl(); ?>" target="_blank" class="button--with-icon-vertical">
                                            <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <p>&copy;<?= new \Backstage\Support\DateTime('now', 'Y'); ?> Bershtein Law LLC</p>
                        <p><a href="#disclaimer">Disclaimer</a></p>
                    </div>
                </div>
                <div class="footer-nav__column col-12 col-sm-6 offset-lg-2">
                    <div class="footer-nav__column-inner">
                        <h2 class="heading heading--medium text--uppercase"><?= new IconView(['icon_name' => 'headphones', 'style' => 'primary']); ?> Quick Contact</h2>
                        <?= Util::getTemplateScoped('templates/components/compact-contact-form.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?= ModalView::unloadAll(); ?>
