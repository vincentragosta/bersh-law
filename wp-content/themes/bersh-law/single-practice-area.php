<?php

use ChildTheme\Options\GlobalOptions;
use ChildTheme\PracticeArea\PracticeArea;

$PracticeArea = PracticeArea::createFromGlobal();
?>

<div class="content-section" style="margin-top: 0">
    <?php if (($image = GlobalOptions::practiceAreaImage()) instanceof WP_Image): /* @var WP_Image $image */ ?>
        <span class="content-background-image">
            <span class="content-background-image__images">
                <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url(<?= $image->url; ?>);"></span>
            </span>
        </span>
    <?php endif; ?>
    <div class="content-section__container container" style="position: relative;">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="responsive-spacer responsive-spacer--10"></div>
                    <div class="responsive-spacer responsive-spacer--7"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($content = $PracticeArea->content(false))): ?>
    <div class="content-section content-section--mb-double" style="font-size: 18px;">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12 <?= !empty($disciplines = $PracticeArea->disciplines) ? 'col-sm-8' : ''; ?>">
                    <div class="content-column__inner" style="padding: var(--global__sizing__layout-spacing--default) 0;">
                        <?= $content; ?>
                    </div>
                </div>
                <?php if (!empty($disciplines = $PracticeArea->disciplines)): ?>
                    <div class="content-column col-12 col-sm-4">
                        <div class="content-column__inner">
                            <div class="discipline-card">
                                <h2 class="heading heading--large heading--primary">Focus Areas</h2>
                                <ul class="list">
                                    <?php foreach($disciplines as $discipline): ?>
                                        <li><?= array_shift($discipline); ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
