<?php

namespace ChildTheme;

use Orchestrator\Theme as ThemeBase;
use ChildTheme\Controller;
use ChildTheme\Components;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_DEFAULT_POST_TYPE = true;

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
    ];

    const EXTENSIONS = [
        Controller\ThemeController::class,
        Controller\EmailController::class,
        Controller\FormRestController::class,
        Controller\VcLibraryController::class,
        Controller\SetDesignController::class,
        Components\IconCard\IconCard::class,
        Components\ImageCard\ImageCard::class,
        Components\HorizontalRuleWithIcon\HorizontalRuleWithIcon::class,
        Components\TeamMemberCard\TeamMemberCard::class,
        Components\Card\Card::class,
        Components\ListCard\ListCard::class,
        Components\SimpleImageCard\SimpleImageCard::class,
        Components\CustomForm\InquiryForm::class,
        Components\PracticeAreaCard\PracticeAreaCard::class,
        Components\CustomForm\ContactForm::class
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Dynalight&family=Roboto:wght@400;700&display=swap', '', null);
    }
}
