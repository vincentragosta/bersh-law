<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class FormException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class FormException extends Exception {}
