<?php
/**
 * Expected:
 * @var string $icon
 * @var string $content
 * @var string $heading_text
 * @var array $link
 * @var string $description
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;

if (empty($content) || empty($heading_text)) {
    return '';
}
?>

<div <?= Util::componentAttributes('list-card', $class_modifiers, $element_attributes); ?>>
    <?php if (!empty($icon)): ?>
        <div class="list-card__icon">
            <?= new IconView(['icon_name' => $icon]); ?>
        </div>
    <?php endif; ?>
    <div class="list-card__heading-container">
        <h3 class="list-card__heading heading heading--default"><?= $heading_text; ?></h3>
        <?php if (!empty($description)): ?>
            <p class="list-card__description text--cursive"><?= $description; ?></p>
        <?php endif; ?>
    </div>
    <div class="list-card__content-container">
        <?= $content; ?>
    </div>
    <div class="list-card__link-container">
        <?php if (!empty($link['url'])): ?>
            <?= Link::createFromField($link)->class('list-card__link button'); ?>
        <?php endif; ?>
    </div>
</div>
