<?php

namespace ChildTheme\Components\ListCard;

use Backstage\View\Component;

/**
 * Class ListCardView
 * @package ChildTheme\Components\ListCard
 * @author Vincent Ragosta <vincentragosta@gmail.com>
 * @version 1.0
 *
 * @property string $icon
 * @property string $content
 * @property string $heading_text
 * @property array $link
 * @property string $description
 */
class ListCardView extends Component
{
    protected $name = 'list-card';
    protected static $default_properties = [
        'icon' => '',
        'content' => '',
        'heading_text' => '',
        'link' => [],
        'description' => '',
    ];

    public function __construct(string $icon, string $content, string $heading_text, array $link = [], string $description = '')
    {
        parent::__construct(compact('icon', 'content', 'heading_text', 'link', 'description'));
    }
}
