<?php

namespace ChildTheme\Components\ListCard;

use Backstage\VcLibrary\Support\Component;

/**
 * Class ListCard
 *
 * @package ChildTheme\Components\ListCard
 *
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ListCard extends Component
{
    const NAME = 'List Card';
    const TAG = 'list_card';
    const VIEW = ListCardView::class;

    protected $component_config = [
        'description' => 'Create an image with heading and link.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'dropdown',
                'heading' => 'Icon',
                'param_name' => 'icon',
                'value' => [
                    '-- Select Icon --' => '',
                    'Balance' => 'balance',
                    'Banknote' => 'banknote',
                    'Building 1' => 'building-1',
                    'Building 2' => 'building-2',
                    'Building 3' => 'building-3',
                    'Building 4' => 'building-4',
                    'Building 5' => 'building-5',
                    'Building 6' => 'building-6',
                    'Cursor' => 'cursor',
                    'Edit' => 'edit',
                    'Gavel' => 'gavel',
                    'Note' => 'note',
                    'Log out' => 'log-out',
                    'Script 1' => 'script-1',
                    'Script 2' => 'script-2',
                    'User 1' => 'user-1',
                    'User 2' => 'user-2'
                ],
                'description' => 'Select an Icon',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Heading text',
                'param_name' => 'heading_text',
                'description' => 'Enter a heading.',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Descriptive Text',
                'param_name' => 'description',
                'description' => 'Add descriptive text.'
            ],
            [
                'type' => 'textarea_html',
                'heading' => 'Build List Items',
                'param_name' => 'content',
                'description' => 'Construct the list using RAW HTML.'
            ],
            [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'description' => 'Set the link'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /* @var ListCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass($atts['icon'], $atts['content'], $atts['heading_text'], vc_build_link($atts['link']), $atts['description']);
    }
}
