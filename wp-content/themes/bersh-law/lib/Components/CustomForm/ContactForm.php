<?php

namespace ChildTheme\Components\CustomForm;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;

/**
 * Class ContactForm
 * @package ChildTheme\Components\CustomForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ContactForm extends Component
{
    const NAME = 'Contact Form';
    const TAG = 'contact-form';

    protected $component_config = [
        'description' => 'Drop the contact form template.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'show_settings_on_create' => false,
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => []
    ];

    protected function createView(array $atts)
    {
        return Util::getTemplateScoped('templates/components/contact-form.php');
    }
}
