<?php

namespace ChildTheme\Components\CustomForm;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;

/**
 * Class InquiryForm
 * @package ChildTheme\Components\CustomForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class InquiryForm extends Component
{
    const NAME = 'Inquiry Form';
    const TAG = 'inquiry-form';

    protected $component_config = [
        'description' => 'Drop the inquiry form template.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'show_settings_on_create' => false,
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => []
    ];

    protected function createView(array $atts)
    {
        return Util::getTemplateScoped('templates/components/inquiry-form.php');
    }
}
