<?php

namespace ChildTheme\Components\PracticeAreaCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Components\ImageCard\ImageCardView;
use ChildTheme\PracticeArea\PracticeArea;
use ChildTheme\PracticeArea\PracticeAreaRepository;

/**
 * Class PracticeAreaCard
 * @package ChildTheme\Components\PracticeAreaCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PracticeAreaCard extends Component
{
    const NAME = 'Practice Area Card';
    const TAG = 'practice_area_card';
    const VIEW = ImageCardView::class;

    protected $component_config = [
        'description' => 'Create an image with heading and link from a Practice Area.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'icons' => [
                'type' => 'dropdown',
                'heading' => 'Icon',
                'param_name' => 'icon',
                'value' => [
                    '-- Select Icon --' => '',
                    'Balance' => 'balance',
                    'Banknote' => 'banknote',
                    'Building 1' => 'building-1',
                    'Building 2' => 'building-2',
                    'Building 3' => 'building-3',
                    'Building 4' => 'building-4',
                    'Building 5' => 'building-5',
                    'Building 6' => 'building-6',
                    'Cursor' => 'cursor',
                    'Edit' => 'edit',
                    'Gavel' => 'gavel',
                    'Note' => 'note',
                    'Log out' => 'log-out',
                    'Script 1' => 'script-1',
                    'Script 2' => 'script-2',
                    'User 1' => 'user-1',
                    'User 2' => 'user-2'
                ],
                'description' => 'Select an Icon',
                'admin_label' => true
            ],
            'practice_areas' => [
                'type' => 'dropdown',
                'heading' => 'Practice Areas',
                'param_name' => 'practice_area_id',
                'value' => '',
                'description' => 'Select a Practice Area',
                'admin_label' => true
            ]
        ]
    ];

    protected function setupConfig()
    {
        parent::setupConfig();
        $this->setPracticeAreas();
    }

    protected function setPracticeAreas()
    {
        $options['-- Select Practice Area --'] = '';
        $PracticeAreaRepository = new PracticeAreaRepository();
        $practice_areas = $PracticeAreaRepository->findAll();
        foreach ($practice_areas as $PracticeArea) {
            /** @var PracticeArea $PracticeArea */
            $options[$PracticeArea->post()->post_title] = $PracticeArea->ID;
        }
        $this->component_config['params']['practice_areas']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        if (empty($atts['practice_area_id'])) {
            return '';
        }
        /* @var ImageCardView $ViewClass */
        $ViewClass = static::VIEW;
        return $ViewClass::createFromPracticeArea(new PracticeArea($atts['practice_area_id']), $atts['icon']);
    }
}
