<?php

namespace ChildTheme\Components\HorizontalRuleWithIcon;

use Backstage\View\Component;

/**
 * Class HorizontalRuleWithIconView
 * @package ChildTheme\Components\HorizontalRuleWithIcon
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $icon
 */
class HorizontalRuleWithIconView extends Component
{
    protected $name = 'horizontal-rule-with-icon.php';
    protected static $default_properties = [
        'icon' => ''
    ];

    public function __construct(string $icon)
    {
        parent::__construct(compact('icon'));
    }
}
