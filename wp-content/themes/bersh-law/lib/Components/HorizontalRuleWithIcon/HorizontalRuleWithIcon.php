<?php

namespace ChildTheme\Components\HorizontalRuleWithIcon;

use Backstage\VcLibrary\Support\Component;

/**
 * Class HorizontalRuleWithIcon
 * @package ChildTheme\Components\HorizontalRuleWithIcon
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class HorizontalRuleWithIcon extends Component
{
    const NAME = 'Horizontal Rule With Icon';
    const TAG = 'horizontal_rule_icon_card';
    const VIEW = HorizontalRuleWithIconView::class;

    protected $component_config = [
        'description' => 'Create an icon with a horizontal rule on each side.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'dropdown',
                'heading' => 'Icon',
                'param_name' => 'icon',
                'value' => [
                    '-- Select Icon --' => '',
                    'Balance' => 'balance',
                    'Banknote' => 'banknote',
                    'Building 1' => 'building-1',
                    'Building 2' => 'building-2',
                    'Building 3' => 'building-3',
                    'Building 4' => 'building-4',
                    'Building 5' => 'building-5',
                    'Building 6' => 'building-6',
                    'Cursor' => 'cursor',
                    'Edit' => 'edit',
                    'Gavel' => 'gavel',
                    'Note' => 'note',
                    'Log out' => 'log-out',
                    'Script 1' => 'script-1',
                    'Script 2' => 'script-2',
                    'User 1' => 'user-1',
                    'User 2' => 'user-2'
                ],
                'description' => 'Select an Icon',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /* @var HorizontalRuleWithIconView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass($atts['icon']);
    }
}
