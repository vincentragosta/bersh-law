<?php
/**
 * Expected:
 * @var string $icon
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;use Backstage\Util;
if (empty($icon)) {
    return '';
}
?>

<div <?= Util::componentAttributes('horizontal-rule-with-icon', $class_modifiers, $element_attributes); ?>>
    <hr class="horizontal-rule-with-icon__hr horizontal-line" />
    <?= new IconView(['icon_name' => $icon, 'style' => 'primary']); ?>
    <hr class="horizontal-rule-with-icon__hr horizontal-line" />
</div>
