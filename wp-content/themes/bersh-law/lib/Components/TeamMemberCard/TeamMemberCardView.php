<?php

namespace ChildTheme\Components\TeamMemberCard;

use Backstage\View\Component;
use ChildTheme\TeamMember\TeamMember;

/**
 * Class TeamMemberCardView
 * @package ChildTheme\Components\TeamMemberCard
 * @author Vincent Ragosta <vincentragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property string $heading_text
 * @property string $description
 * @property string $url
 */
class TeamMemberCardView extends Component
{
    const IMAGE_SIZE = 768;

    protected $name = 'team-member';
    protected static $default_properties = [
        'image' => false,
        'heading_text' => '',
        'description' => '',
        'url' => ''
    ];

    public function __construct(TeamMember $TeamMember)
    {
        parent::__construct([
            'image' => $TeamMember->featuredImage(),
            'heading_text' => $TeamMember->title(),
            'description' => $TeamMember->role,
            'url' => $TeamMember->permalink()
        ]);
        if ($this->image instanceof \WP_Image) {
            $this->image->height(static::IMAGE_SIZE)->width(static::IMAGE_SIZE);
        }
    }
}
