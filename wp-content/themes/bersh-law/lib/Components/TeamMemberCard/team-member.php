<?php
/**
 * Expected:
 * @var WP_Image|bool $image
 * @var string $heading_text
 * @var string $description
 * @var string $url
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;

if (!$image instanceof WP_image || empty($heading_text) || empty($url)) {
    return '';
}
?>

<div <?= Util::componentAttributes('image-card', $class_modifiers, $element_attributes); ?>>
    <div class="image-card__image-container">
        <a href="<?= $url; ?>">
            <?= $image->css_class('image-card__image'); ?>
        </a>
    </div>
    <div class="image-card__drawer">
        <h3 class="image-card__heading heading heading--default"><?= $heading_text; ?></h3>
        <?php if (!empty($description)): ?>
            <p class="image-card__description text--cursive"><?= $description; ?></p>
        <?php endif; ?>
    </div>
</div>
