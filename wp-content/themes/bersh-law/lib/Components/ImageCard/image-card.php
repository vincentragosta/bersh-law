<?php
/**
 * Expected:
 * @var WP_Image|bool $image
 * @var string $icon
 * @var array $link
 * @var string $heading_text
 * @var string $description
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;

if (!$image instanceof WP_image || empty($heading_text)) {
    return '';
}
?>

<div <?= Util::componentAttributes('image-card', $class_modifiers, $element_attributes); ?>>
    <?php if (!empty($icon)): ?>
        <?php if (isset($link['url'])): ?>
            <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>">
                <div class="image-card__icon">
                    <?= new IconView(['icon_name' => $icon]); ?>
                </div>
            </a>
        <?php else: ?>
            <div class="image-card__icon">
                <?= new IconView(['icon_name' => $icon]); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="image-card__image-container">
        <?php if (isset($link['url'])): ?>
            <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>">
                <?= $image->css_class('image-card__image'); ?>
            </a>
        <?php else: ?>
            <?= $image->css_class('image-card__image'); ?>
        <?php endif; ?>
    </div>
    <div class="image-card__drawer">
        <h3 class="image-card__heading heading heading--default"><?= $heading_text; ?></h3>
        <?php if (!empty($description)): ?>
            <p class="image-card__description text--cursive"><?= $description; ?></p>
        <?php endif; ?>
        <?php if (!empty($link) && isset($link['url']) && isset($link['title'])): ?>
            <?= Link::createFromField($link)->class('image-card__link button'); ?>
        <?php endif; ?>
    </div>
</div>
