<?php

namespace ChildTheme\Components\ImageCard;

use Backstage\VcLibrary\Support\Component;

/**
 * Class ImageCard
 * @package ChildTheme\Components\ImageCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ImageCard extends Component
{
    const NAME = 'Image Card';
    const TAG = 'image_card';
    const VIEW = ImageCardView::class;

    protected $component_config = [
        'description' => 'Create an image with heading and link.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image_id',
                'description' => 'Set the image.'
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Icon',
                'param_name' => 'icon',
                'value' => [
                    '-- Select Icon --' => '',
                    'Balance' => 'balance',
                    'Banknote' => 'banknote',
                    'Building 1' => 'building-1',
                    'Building 2' => 'building-2',
                    'Building 3' => 'building-3',
                    'Building 4' => 'building-4',
                    'Building 5' => 'building-5',
                    'Building 6' => 'building-6',
                    'Cursor' => 'cursor',
                    'Edit' => 'edit',
                    'Gavel' => 'gavel',
                    'Note' => 'note',
                    'Log out' => 'log-out',
                    'Script 1' => 'script-1',
                    'Script 2' => 'script-2',
                    'User 1' => 'user-1',
                    'User 2' => 'user-2'
                ],
                'description' => 'Select an Icon',
                'admin_label' => true
            ],
            [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'description' => 'Set the link'
            ],
            [
                'type' => 'textfield',
                'heading' => 'Heading text',
                'param_name' => 'heading_text',
                'description' => 'Enter a heading.',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Descriptive Text',
                'param_name' => 'description',
                'description' => 'Add descriptive text.'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /* @var ImageCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(\WP_Image::get_by_attachment_id($atts['image_id']), $atts['heading_text'], $atts['icon'], vc_build_link($atts['link']), $atts['description']);
    }
}
