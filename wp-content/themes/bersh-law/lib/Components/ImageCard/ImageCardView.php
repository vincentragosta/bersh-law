<?php

namespace ChildTheme\Components\ImageCard;

use Backstage\View\Component;
use ChildTheme\PracticeArea\PracticeArea;
use ChildTheme\TeamMember\TeamMember;

/**
 * Class ImageCardView
 * @package ChildTheme\Components\ImageCard
 * @author Vincent Ragosta <vincentragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property string $icon
 * @property array $link
 * @property string $heading_text
 * @property string $description
 */
class ImageCardView extends Component
{
    const IMAGE_SIZE = 768;

    protected $name = 'image-card';
    protected static $default_properties = [
        'image' => false,
        'heading_text' => '',
        'icon' => '',
        'link' => [],
        'description' => ''
    ];

    public function __construct($image, string $heading_text, string $icon = '', array $link = [], string $description = '')
    {
        parent::__construct(compact('image', 'heading_text', 'icon', 'link', 'description'));
        if ($this->image instanceof \WP_Image) {
            $this->image->height(static::IMAGE_SIZE)->width(static::IMAGE_SIZE);
        }
    }

    public static function createFromTeamMember(TeamMember $TeamMember)
    {
        return new static(
            $TeamMember->featuredImage(),
            $TeamMember->title(),
            '',
            [
                'title' => 'Learn More',
                'url' => $TeamMember->permalink()
            ],
            $TeamMember->role
        );
    }

    public static function createFromPracticeArea(PracticeArea $PracticeArea, string $icon = '')
    {
        return new static(
            $PracticeArea->featuredImage(),
            $PracticeArea->title(),
            $icon,
            [
                'title' => 'Learn More',
                'url' => $PracticeArea->permalink()
            ]
        );
    }
}
