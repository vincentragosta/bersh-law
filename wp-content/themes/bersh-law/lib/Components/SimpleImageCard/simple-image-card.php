<?php
/**
 * Expected:
 * @var string $heading_text
 * @var WP_Image|bool $image
 * @var string $content
 * @var array $link
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (!$image instanceof WP_image) {
    return '';
}
?>

<div <?= Util::componentAttributes('simple-image-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($heading_text): ?>
        <?php if (!empty($link) && isset($link['url'])): ?>
            <h3 class="simple-image-card__heading heading heading--medium">
                <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>">
                    <?= $heading_text; ?>
                </a>
            </h3>
        <?php else: ?>
            <h3 class="simple-image-card__heading heading heading--medium"><?= $heading_text; ?></h3>
        <?php endif; ?>
    <?php endif; ?>
    <div class="simple-image-card__image-container">
        <?php if (!empty($link) && isset($link['url'])): ?>
            <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>"><?= $image->css_class('simple-image-card__image'); ?></a>
        <?php else: ?>
            <?= $image->css_class('simple-image-card__image'); ?>
        <?php endif; ?>
    </div>
    <?php if ($content): ?>
        <div class="simple-image-card__content">
            <?= $content; ?>
        </div>
    <?php endif; ?>
</div>
