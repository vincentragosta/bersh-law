<?php

namespace ChildTheme\Components\SimpleImageCard;

use Backstage\View\Component;

/**
 * Class SimpleImageCardView
 * @package ChildTheme\Components\ImageCard
 * @author Vincent Ragosta <vincentragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property string $content
 * @property string $heading_text
 * @property array $link
 */
class SimpleImageCardView extends Component
{
    const IMAGE_SIZE = 768;

    protected $name = 'simple-image-card';
    protected static $default_properties = [
        'image' => false,
        'content' => '',
        'heading_text' => '',
        'link' => []
    ];

    public function __construct($image, string $content, string $heading_text = '', array $link = [])
    {
        parent::__construct(compact('image', 'content', 'heading_text', 'link'));
        if ($this->image instanceof \WP_Image) {
            $this->image->height(static::IMAGE_SIZE)->width(static::IMAGE_SIZE);
        }
    }
}
