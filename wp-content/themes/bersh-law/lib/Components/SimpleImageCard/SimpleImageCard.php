<?php

namespace ChildTheme\Components\SimpleImageCard;

use Backstage\VcLibrary\Support\Component;

/**
 * Class SimpleImageCard
 * @package ChildTheme\Components\SimpleImageCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SimpleImageCard extends Component
{
    const NAME = 'Simple Image Card';
    const TAG = 'simple_image_card';
    const VIEW = SimpleImageCardView::class;

    protected $component_config = [
        'description' => 'Create an image with heading and descriptive text.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image_id',
                'description' => 'Set the image.'
            ],
            [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'description' => 'Set an optional link.'
            ],
            [
                'type' => 'textfield',
                'heading' => 'Heading',
                'param_name' => 'heading_text',
                'description' => 'Set an optional heading.',
                'admin_label' => true
            ],
            [
                'type' => 'textarea_html',
                'heading' => 'Content',
                'param_name' => 'content',
                'description' => 'Enter the content.',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /* @var SimpleImageCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(\WP_Image::get_by_attachment_id($atts['image_id']), $atts['content'], $atts['heading_text'], vc_build_link($atts['link']));
    }
}
