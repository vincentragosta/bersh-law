<?php

namespace ChildTheme\Components\IconCard;

use Backstage\View\Component;

/**
 * Class IconCardView
 * @package ChildTheme\Components\IconCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $icon
 * @property string $url
 * @property string $heading_text
 */
class IconCardView extends Component
{
    protected $name = 'icon-card';
    protected static $default_properties = [
        'icon' => '',
        'url' => '',
        'heading_text' => ''
    ];

    public function __construct(string $icon, string $url, string $heading_text = '')
    {
        parent::__construct(compact('icon', 'url', 'heading_text'));
    }
}
