<?php
/**
 * Expected:
 * @var string $icon
 * @var string $heading_text
 * @var string $url
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;

if (empty($icon) || empty($url)) {
    return;
}
?>

<div <?= Util::componentAttributes('icon-card', $class_modifiers, $element_attributes); ?>>
    <div class="icon-card__icon">
        <a href="<?= $url; ?>" class="icon-card__link">
            <?= new IconView(['icon_name' => $icon]); ?>
        </a>
    </div>
    <?php if (!empty($heading_text)): ?>
        <h3 class="icon-card__heading heading heading--default">
            <a href="<?= $url; ?>" class="icon-card__link">
                <?= $heading_text; ?>
                <hr class="icon-card__hr horizontal-line" />
            </a>
        </h3>
    <?php endif; ?>
</div>
