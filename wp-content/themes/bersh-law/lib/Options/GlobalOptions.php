<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use WP_Image;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @method static WP_Image logo()
 * @method static contactAddress()
 * @method static googleReviewLink()
 * @method static companyName()
 * @method static contactEmail()
 * @method static contactPhone()
 * @method static socialIcons()
 * @method static practiceAreaImage()
 * @method static headerEmailText()
 * @method static headerPhoneText()
 * @method static headerCta()
 * @method static clientPortalUrl()
 * @method static footerText()
 * @method static disclaimerText()
 */
class GlobalOptions extends OptionsBase
{
    protected $default_values = [
        'logo' => null,
        'contact_address' => '',
        'company_name' => '',
        'contact_email' => '',
        'contact_phone' => '',
        'social_icons' => [],
        'practice_area_image' => null,
        'header_email_text' => '',
        'header_phone_text' => '',
        'header_cta' => [],
        'client_portal_url' => '',
        'footer_text' => '',
        'disclaimer_text' => ''
    ];

    protected function getLogo()
    {
        $logo = $this->get('logo');
        return WP_Image::get_by_attachment_id($logo);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->get('social_icons'));
    }

    protected function getPracticeAreaImage()
    {
        $practice_area_image = $this->get('practice_area_image');
        return WP_Image::get_by_attachment_id($practice_area_image);
    }
}
