<?php

namespace ChildTheme\Controller;

use Backstage\Controller\RestController;
use ChildTheme\Exception\FormException;use ChildTheme\Options\GlobalOptions;

/**
 * Class FormRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $namespace
 */
class FormRestController extends RestController
{
    const SUBJECT = 'You have a new contact request!';
    const COMPACT_SUBJECT = 'You have a new contact request!';
    const EMAIL_TEMPLATE = "
    Full Name: %s
    Email: %s
    Phone: %s
    State: %s

    %s";
    const COMPACT_EMAIL_TEMPLATE = "
    Email: %s

    %s";
    protected $namespace = 'form';

    public function registerRoutes()
    {
        $this->addCreateRoute('/contact/submit', [$this, 'submitFormEntry']);
        $this->addCreateRoute('/compact/submit', [$this, 'compactSubmitFormEntry']);
    }

    public function submitFormEntry(\WP_REST_Request $request)
    {
        if (empty($full_name = $request->get_param('fullName'))) {
            throw new FormException('The full name was not sent in the request.', 400);
        }

        if (empty($email = $request->get_param('email'))) {
            throw new FormException('The email was not sent in the request.', 400);
        }

        if (empty($phone = $request->get_param('phone'))) {
            throw new FormException('The phone was not sent in the request.', 400);
        }

        if (empty($state = $request->get_param('state'))) {
            throw new FormException('The state was not sent in the request.', 400);
        }

        if (empty($description = $request->get_param('description'))) {
            throw new FormException('The description was not sent in the request.', 400);
        }

        if (empty($to = GlobalOptions::contactEmail())) {
            throw new FormException('The contact email address is not set in the backend.', 400);
        }
        if (!wp_mail($to, static::SUBJECT, sprintf(static::EMAIL_TEMPLATE, $full_name, $email, $phone, $state, $zip, $description))) {
            throw new FormException('Something went wrong when sending the email.', 400);
        }
        return ['message' => 'The email was successfully sent.', 'status' => 200];
    }

    public function compactSubmitFormEntry(\WP_REST_Request $request)
    {
        if (empty($email = $request->get_param('email'))) {
            throw new FormException('The email was not sent in the request.', 400);
        }

        if (empty($description = $request->get_param('description'))) {
            throw new FormException('The description was not sent in the request.', 400);
        }

        if (empty($to = GlobalOptions::contactEmail())) {
            throw new FormException('The contact email address is not set in the backend.', 400);
        }
        if (!wp_mail($to, static::COMPACT_SUBJECT, sprintf(static::COMPACT_EMAIL_TEMPLATE, $email, $description))) {
            throw new FormException('Something went wrong when sending the email.', 400);
        }
        return ['message' => 'The email was successfully sent.', 'status' => 200];
    }
}
