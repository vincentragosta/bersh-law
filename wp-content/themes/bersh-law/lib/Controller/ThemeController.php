<?php

namespace ChildTheme\Controller;

use ChildTheme\Options\GlobalOptions;
use ChildTheme\User\User;

/**
 * Class ThemeController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ThemeController
{
    public function __construct()
    {
        add_filter('global_js_vars', [$this, 'updateJsVars'], 10, 1);
        add_action('template_redirect', [$this, 'templateRedirect']);
    }

    public function updateJSVars($js_vars)
    {
        $args = [];
        if (defined('BASIC_AUTH_CREDENTIALS')) {
            $args['basic_auth'] = BASIC_AUTH_CREDENTIALS;
        }
        return array_merge($js_vars, $args);
    }

    public function templateRedirect()
    {
        if (is_page_template('templates/construction.php')) {
            add_filter('show_site_header', '__return_false');
            add_filter('show_site_footer', '__return_false');
        }
    }
}
