<?php

namespace ChildTheme\Controller;

/**
 * Class SetDesignController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SetDesignController
{
    public function __construct()
    {
        add_filter('set-design/accordion/icon-properties', [$this, 'accordionIconProperties']);
    }

    public function accordionIconProperties(array $properties)
    {
        $properties['direction'] = 'left';
        return $properties;
    }
}
