<?php

namespace ChildTheme\Controller;

/**
 * Class VcLibraryController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class VcLibraryController
{
    protected static $component_blacklist = [
        'content_card',
        'content_block',
        'callout',
        'sub-navigation',
        'ticket_calendar',
        'talent_card',
        'media_carousel',
        'image_carousel',
        'tabs',
        'video',
        'gravityform'
    ];

    protected static $additional_background_colors = [
        'Black' => 'black',
        'White' => 'white',
        'Darker' => 'darker',
        'Gray Light' => 'gray-light',
        'Gray Lighter' => 'gray-lighter'
    ];

    public function __construct()
    {
        add_filter('backstage/vc-library/background-colors/vc_section', [$this, 'addBackgroundColors']);
        add_filter('backstage/vc-library/background-colors/vc_column', [$this, 'addBackgroundColors']);
        add_filter('backstage/vc-library/blacklist', [$this, 'blacklist']);
    }

    public function addBackgroundColors($colors) {
        return array_merge($colors, static::$additional_background_colors);
    }

    public function blacklist($components)
    {
        return array_merge($components, static::$component_blacklist);
    }
}
