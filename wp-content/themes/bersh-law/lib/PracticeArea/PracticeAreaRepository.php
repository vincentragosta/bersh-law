<?php

namespace ChildTheme\PracticeArea;

use Backstage\Repositories\PostRepository;

/**
 * Class PracticeAreaRepository
 * @package ChildTheme\PracticeArea
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PracticeAreaRepository extends PostRepository
{
    protected $model_class = PracticeArea::class;
}
