<?php

namespace ChildTheme\PracticeArea;

use Backstage\Models\PostBase;

/**
 * Class PracticeArea
 * @package ChildTheme\PracticeArea
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property array $disciplines
 */
class PracticeArea extends PostBase
{
    const POST_TYPE = 'practice-area';
}
