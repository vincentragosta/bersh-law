<?php

namespace ChildTheme\TeamMember;

use Backstage\Models\PostBase;

/**
 * Class TeamMember
 * @package ChildTheme\TeamMember
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $role
 * @property string $languages
 * @property string $education
 * @property string $admissions
 * @property array $social_channels
 * @property array $contact_information
 */
class TeamMember extends PostBase
{
    const POST_TYPE = 'team-member';
}
