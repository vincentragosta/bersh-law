<?php

use Backstage\SetDesign\Accordion\AccordionPanelView;
use Backstage\SetDesign\Accordion\AccordionView;
use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\View\Element;
use ChildTheme\TeamMember\TeamMember;

$TeamMember = TeamMember::createFromGlobal();
$accordion_panels = [];

if (!empty($education = $TeamMember->education)) {
    $accordion_panels[] = new AccordionPanelView(Element::create('h3', sprintf('%s %s', new IconView(['icon_name' => 'hat']), 'Education'), ['class' => 'heading heading--medium']), $education);
}
if (!empty($admissions = $TeamMember->admissions)) {
    $accordion_panels[] = new AccordionPanelView(Element::create('h3', sprintf('%s %s', new IconView(['icon_name' => 'building']), 'Admissions'), ['class' => 'heading heading--medium']), $admissions);
}
if (!empty($languages = $TeamMember->languages)) {
    $accordion_panels[] = new AccordionPanelView(Element::create('h3', sprintf('%s %s', new IconView(['icon_name' => 'language']), 'Languages'), ['class' => 'heading heading--medium']), $languages);
}
if (!empty($contact_information = $TeamMember->contact_information)) {
    if (isset($contact_information['email']) && !empty($contact_information['email'])) {
        $contact_information['email'] = sprintf('Email: <a href="mailto:%1$s">%1$s</a>', $contact_information['email']);
    }
    if (isset($contact_information['phone']) && !empty($contact_information['phone'])) {
        $contact_information['phone'] = sprintf('Phone: <a href="tel:%1$s">%1$s</a>', $contact_information['phone']);
    }
    $accordion_panels[] = new AccordionPanelView(Element::create('h3', sprintf('%s %s', new IconView(['icon_name' => 'headphones']), 'Contact'), ['class' => 'heading heading--medium']), implode('<br />', $contact_information));
}
?>

<div class="content-section content-section--has-bg content-section--tpad-double content-section--bpad-double content-section--darker content-section--mb-double">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <h1 class="heading heading--large heading--inverted"><?= $TeamMember->title(); ?></h1>
                    <?php if (!empty($role = $TeamMember->role)): ?>
                        <p class="heading heading--medium heading--inverted"><?= $TeamMember->role; ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--mb-double">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12 col-sm-5 text--center">
                <div class="content-column__inner">
                    <?php if (($image = $TeamMember->featuredImage()) instanceof \WP_Image): ?>
                        <?= $image->width(768)->height(768); ?>
                    <?php endif; ?>
                    <div style="display: flex; justify-content: space-between;">
                        <div style="display: flex; flex-flow: column; align-items: flex-start;">
                            <h2 class="heading heading--medium"><?= $TeamMember->title(); ?></h2>
                            <?php if (!empty($role = $TeamMember->role)): ?>
                                <p><?= $TeamMember->role; ?></p>
                            <?php endif; ?>
                        </div>
                        <?php if (!empty($social_channels = $TeamMember->social_channels)): ?>
                            <ul class="list list--inline">
                                <?php foreach(array_map(function($row) {
                                    return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $row['link']['title']);
                                }, $social_channels) as $SocialIcon): ?>
                                    <li>
                                        <a href="<?= $SocialIcon->getUrl(); ?>" target="_blank" class="button--with-icon-vertical">
                                            <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                                            <small><?= $SocialIcon->getLabel(); ?></small>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($accordion_panels)): ?>
                        <?= new AccordionView($accordion_panels); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="content-column col-12 col-sm-7">
                <div class="content-column__inner">
                    <?php if (!empty($content = $TeamMember->content(false))): ?>
                        <div class="column-text" style="font-size: 18px;">
                            <?= $content; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
