<?php

use ChildTheme\Components\ImageCard\ImageCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\PracticeArea\PracticeArea;
use ChildTheme\PracticeArea\PracticeAreaRepository;

?>

<div class="content-section" style="margin-top: 0;">
    <?php if (($image = GlobalOptions::practiceAreaImage()) instanceof WP_Image): /* @var WP_Image $image */ ?>
        <span class="content-background-image">
            <span class="content-background-image__images">
                <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url(<?= $image->url; ?>);"></span>
            </span>
        </span>
    <?php endif; ?>
    <div class="content-section__container container" style="position: relative;">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="responsive-spacer responsive-spacer--6"></div>
                    <h1 class="heading heading--xlarge heading--inverted">Practice Areas</h1>
                    <div class="responsive-spacer responsive-spacer--6"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($practice_areas = (new PracticeAreaRepository())->find(['posts_per_page' => -1, 'order' => 'ASC']))): ?>
    <div class="content-section content-section--has-bg content-section--tpad-double content-section--bpad-double">
        <div class="content-section__container container">
            <div class="content-row row">
                <?php foreach($practice_areas as $index => $PracticeArea): /* @var PracticeArea $PracticeArea */ ?>
                    <?php if ($index == 3): ?>
                        </div>
                        <div class="content-row row">
                    <?php endif; ?>
                    <div class="content-column col-12 col-md-4 col-lg-3">
                        <div class="content-column__inner">
                            <?= ImageCardView::createFromPracticeArea($PracticeArea); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
