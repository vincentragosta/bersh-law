addAction(INIT, function () {
    SetDesign.addFilter('content-slider/owl-settings', function (settings) {
        settings.nav = true;
        settings.dots = false;
        settings.navText = ['<span class="icon icon-direction-up"><svg><use class="icon-use" xlink:href="#icon-chevron"></use></svg></span>', '<span class="icon"><svg><use class="icon-use" xlink:href="#icon-chevron"></use></svg></span>'];
        return settings;
    });
});

// addAction(SetDesign.READY, function () {
//     var $tickerTapeSliderContainer = $('.content-section--ticker-tape-slider');
//     $tickerTapeSliderContainer.find('.content-column__inner').addClass('owl-carousel');
//     var $tickerTapeSlider = $tickerTapeSliderContainer.find('.owl-carousel');
//     if (!$tickerTapeSlider.length) {
//         return;
//     }
//     $tickerTapeSlider.owlCarousel({
//         loop: true,
//         nav: false,
//         navText: '',
//         dots: false,
//         autoplay: true,
//         slideTransition: 'linear',
//         autoplaySpeed: 6000,
//         smartSpeed: 6000,
//         center: true,
//         mouseDrag: false,
//         touchDrag: false,
//         pullDrag: false,
//         responsive: {
//             0: {
//                 items: 2
//             },
//             480: {
//                 items: 3
//             },
//             768: {
//                 item: 4
//             },
//             1100: {
//                 items: 6
//             }
//         }
//     });
//     $tickerTapeSlider.trigger('play.owl.autoplay', [2000]);
//     console.log('test');
//
//     function setSpeed() {
//         $tickerTapeSlider.trigger('play.owl.autoplay', [6000]);
//     }
//
//     setTimeout(setSpeed, 1000);
// });
