addAction(INIT, function() {
   var $contactForm = $('#compact-contact-form');
   if (!$contactForm.length) {
       return;
   }
   $contactForm.submit(function(e) {
       e.preventDefault();

       var formData = new FormData();
       var email = $contactForm.find('#compact-contact-form__email').val();
       var description = $contactForm.find('#compact-contact-form__description').val();

       formData.append('email', email);
       formData.append('description', description);

       $('html, body').css('cursor', 'wait');

       $.ajax({
           method: 'POST',
           url: sit.api_url + 'form/compact/submit',
           data: formData,
           processData: false,
           contentType: false,
           beforeSend: function (xhr) {
               xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
               xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
           },
           success: function (response) {
               $('html, body').css('cursor', 'auto');
               $contactForm.addClass('custom-form--success');
               $contactForm.find('input[type="submit"]').val('Email Sent').prop('disabled', 'disabled');
           },
           error: function (response) {
               $('html, body').css('cursor', 'auto');
           },
           fail: function (response) {
               $('html, body').css('cursor', 'auto');
           }
       });
   });
});
