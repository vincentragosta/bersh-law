addAction(INIT, function() {
    var $formFields = $('input[type="text"],input[type="email"],textarea');
    if (!$formFields.length) {
        return;
    }

    $formFields.each(function(index, formField) {
        var $formField = $(formField);
        var formClass = '.' + $formField[0].classList[0];
        $formField.on('focus', function() {
            if ($(this)[0].placeholder.length) {
                var placeholder = $(this)[0].placeholder;
                $(this)[0].placeholder = '';
                $(document).bind('click', function(e) {
                    if(!$(e.target).is(formClass)) {
                        $(formClass)[0].placeholder = placeholder;
                    }
                });
            }
            if ($(this)[0].defaultValue.length) {
                var value = $(this)[0].defaultValue;
                $(this)[0].defaultValue = '';
                $(document).bind('click', function(e) {
                    if(!$(e.target).is(formClass)) {
                        $(formClass).text(value);
                    }
                });
            }
        });
    });
});
