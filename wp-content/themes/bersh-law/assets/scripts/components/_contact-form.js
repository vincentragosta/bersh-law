addAction(INIT, function () {
    var $contactForm = $('#contact-form');
    if (!$contactForm.length) {
        return;
    }

    $('#contact-form__phone').on('keyup', function () {
        var value = $(this).val();
        input = value.replace(/\D/g, '');
        input = input.substring(0, 10);
        var size = input.length;
        if (size === 0) {
            input = input;
        } else if (size < 4) {
            input = '(' + input;
        } else if (size < 7) {
            input = '(' + input.substring(0, 3) + ') ' + input.substring(3, 6);
        } else {
            input = '(' + input.substring(0, 3) + ') ' + input.substring(3, 6) + ' - ' + input.substring(6, 10);
        }
        $(this).val(input);
    });

    $contactForm.submit(function (e) {
        e.preventDefault();

        var formErrors = [];
        var $formErrors = $contactForm.find('.custom-form__errors');
        var $formProgressBar = $contactForm.find('.custom-form__progress-bar');
        var $submit = $contactForm.find('.custom-form__submit');

        var fullName = $contactForm.find('#contact-form__full-name').val();
        var email = $contactForm.find('#contact-form__email').val();
        var phone = $contactForm.find('#contact-form__phone').val();
        var state = $contactForm.find('#contact-form__state option:selected').val();
        var description = $contactForm.find('#contact-form__description').val();
        var disclaimer = $contactForm.find('#contact-form__disclaimer').is(':checked');

        var ROW_INACTIVE_CLASS = 'custom-form__row--inactive';
        var SUBMIT_INACTIVE_CLASS = 'custom-form__submit--inactive';
        var formArray = $(this).serializeArray();

        if (!$formErrors.hasClass(ROW_INACTIVE_CLASS)) {
            $formErrors.addClass(ROW_INACTIVE_CLASS);
        }

        if (!fullName.length) {
            formErrors.push('<li>Please enter your full name.</li>');
        }

        if (!email.length) {
            formErrors.push('<li>Please enter your email address.</li>');
        }

        if (!phone.length) {
            formErrors.push('<li>Please enter your phone number.</li>');
        }

        if (phone.replace(/\D/g,'').length < 10) {
            formErrors.push('<li>Please ensure your phone number has 10 numbers.</li>');
        }

        if (!state.length) {
            formErrors.push('<li>Please enter your state.</li>');
        }

        if (!description.length) {
            formErrors.push('<li>Please explain your legal issue.</li>');
        }

        if (!disclaimer) {
            formErrors.push('<li>Please confirm you have read the <a href="#disclaimer">disclaimer</a>.</li>');
        }

        if (formErrors.length) {
            $("html, body").animate({ scrollTop: "0" });
            $formErrors.find('ul').html(formErrors.join(''));
            $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
            return;
        }

        $submit.prop('disabled', 'disabled');
        $submit.val('Sending...');

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i].name] = formArray[i].value;
        }

        $('html, body').css('cursor', 'wait');
        $formProgressBar.removeClass(ROW_INACTIVE_CLASS);

        $.ajax({
            method: 'POST',
            url: '/',
            data: JSON.stringify(returnArray),
            contentType: 'application/json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    var percentComplete = Math.floor(e.loaded / e.total * 100);
                    $('#custom-form__progress').val(percentComplete);
                };
                return xhr;
            },
            success: function (response) {
                $('html, body').css('cursor', 'auto');
                $formProgressBar.addClass(ROW_INACTIVE_CLASS);
                $submit.val('Submit');
                if (!$submit.hasClass(SUBMIT_INACTIVE_CLASS)) {
                    $submit.addClass(SUBMIT_INACTIVE_CLASS);
                }
            },
            error: function (response) {
                $('html, body').css('cursor', 'auto');
                $formProgressBar.addClass(ROW_INACTIVE_CLASS);
                $formErrors.find('ul').html('<li>The submission has failed. Please wait a moment and try again. If the issue persists, please send an email to <a href="mailto:info@bershlaw.com">Info@BershLaw.com</a></li>');
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
                $submit.removeAttr('disabled');
                $submit.val('Submit');
            },
            fail: function (response) {
                $('html, body').css('cursor', 'auto');
                $formProgressBar.addClass(ROW_INACTIVE_CLASS);
                $formErrors.find('ul').html('<li>The submission has failed. Please wait a moment and try again. If the issue persists, please send an email to <a href="mailto:info@bershlaw.com">Info@BershLaw.com</a></li>');
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
                $submit.removeAttr('disabled');
                $submit.val('Submit');
            }
        });
    });
});
