addAction(INIT, function () {
    var $inquiryForm = $('#inquiry-form');
    if (!$inquiryForm.length) {
        return;
    }

    var $formErrors = $inquiryForm.find('.custom-form__errors');
    var $nextStep = $inquiryForm.find('#inquiry-form__next-step');
    var $previousStep = $inquiryForm.find('#inquiry-form__previous-step');
    var $stepDisplay = $inquiryForm.find('#inquiry-form__step-display');
    var $submit = $inquiryForm.find('.custom-form__submit');

    if (
        !$formErrors.length ||
        !$nextStep.length ||
        !$previousStep.length ||
        !$stepDisplay.length ||
        !$submit.length
    ) {
        return;
    }

    var $step1,$step2,$step3,$step4 = '';
    var ROW_INACTIVE_CLASS = 'custom-form__row--inactive';
    var BUTTON_INACTIVE_CLASS = 'custom-form__button--inactive';
    var SUBMIT_INACTIVE = 'custom-form__submit--inactive';
    $previousStep.on('click', function () {
        var stepIndex = parseInt($inquiryForm.attr('data-step-index'));
        var clientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val();
        var curatedClientRole = clientRole.substr(0, 1).toUpperCase() + clientRole.substr(1);

        if (!$formErrors.hasClass(ROW_INACTIVE_CLASS)) {
            $formErrors.addClass(ROW_INACTIVE_CLASS);
        }

        $("html, body").animate({ scrollTop: "0" });

        if (stepIndex === 2) {
            if (!$(this).hasClass(BUTTON_INACTIVE_CLASS)) {
                $(this).addClass(BUTTON_INACTIVE_CLASS);
            }
            $nextStep.text('Next Step: Add Client Information');
            $step1 = $inquiryForm.find('#inquiry-form__step-1');
            $step2 = $inquiryForm.find('#inquiry-form__step-2');
            $step1.toggleClass(ROW_INACTIVE_CLASS, !$step1.hasClass(ROW_INACTIVE_CLASS));
            $step2.toggleClass(ROW_INACTIVE_CLASS, !$step2.hasClass(ROW_INACTIVE_CLASS));
            $inquiryForm.attr('data-step-index', stepIndex - 1);
            $stepDisplay.find('.heading > span').html(stepIndex - 1);
            $stepDisplay.find('.custom-form__row-inner > div').html('<p>This form is for real estate inquiries only. If your matter is not a real estate transaction, please fill out the form on the <a href="/contact/">Contact Us</a> page.</p><p>This is a multi-step form. All fields here are required. Please delineate whether you are an individual or a business entity.</p><p>Please delineate whether the client is an individual or a business entity. If the client is an estate, please select Business Entity as the Client Type. If there are multiple Executors of the estate, please fill out this form as though there are as many sellers as there are representatives.</p>');
        } else if (stepIndex === 3) {
            // $(this).text('Previous Step: Add ' + curatedClientRole + ' Information');
            $(this).text('Previous Step: Client Type');
            $nextStep.text('Next Step: Add ' + (curatedClientRole === 'Buyer' ? 'Seller' : 'Buyer') + ' Information');
            $step2 = $inquiryForm.find('#inquiry-form__step-2');
            $step3 = $inquiryForm.find('#inquiry-form__step-3');
            $step2.toggleClass(ROW_INACTIVE_CLASS, !$step2.hasClass(ROW_INACTIVE_CLASS));
            $step3.toggleClass(ROW_INACTIVE_CLASS, !$step3.hasClass(ROW_INACTIVE_CLASS));
            $inquiryForm.attr('data-step-index', stepIndex - 1);
            $stepDisplay.find('.heading > span').html(stepIndex - 1);
            $stepDisplay.find('.custom-form__row-inner > div').html('Please enter the client’s information below. If there are multiple client being represented, please include each as a separate entry. All fields are required.');
        } else if (stepIndex === 4) {
            if ($nextStep.hasClass(BUTTON_INACTIVE_CLASS)) {
                $nextStep.removeClass(BUTTON_INACTIVE_CLASS);
            }
            $(this).text('Previous Step: Add ' + curatedClientRole + ' Information');
            $nextStep.text('Next Step: Add Property');
            $step3 = $inquiryForm.find('#inquiry-form__step-3');
            $step4 = $inquiryForm.find('#inquiry-form__step-4');
            $step3.toggleClass(ROW_INACTIVE_CLASS, !$step3.hasClass(ROW_INACTIVE_CLASS));
            $step4.toggleClass(ROW_INACTIVE_CLASS, !$step4.hasClass(ROW_INACTIVE_CLASS));
            $submit.toggleClass(SUBMIT_INACTIVE, !$submit.hasClass(SUBMIT_INACTIVE));
            $inquiryForm.attr('data-step-index', stepIndex - 1);
            $stepDisplay.find('.heading > span').html(stepIndex - 1);
            $stepDisplay.find('.custom-form__row-inner > div').html('Please enter the other side’s information. If there are multiple parties on the other side, please include each as a separate entity. All fields are required.');
        }
    });
});
