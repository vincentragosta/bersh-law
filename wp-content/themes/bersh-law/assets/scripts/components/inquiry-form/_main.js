addAction(INIT, function () {
    var $inquiryForm = $('#inquiry-form');
    if (!$inquiryForm.length) {
        return;
    }

    $(document).on('keyup', '[id^=inquiry-form__client-phone-], [id^=inquiry-form__representative-phone-], [id^=inquiry-form__other-client-phone-], [id^=inquiry-form__other-representative-phone-]', function () {
        var value = $(this).val();
        input = value.replace(/\D/g, '');
        input = input.substring(0, 10);
        var size = input.length;
        if (size === 0) {
            input = input;
        } else if (size < 4) {
            input = '(' + input;
        } else if (size < 7) {
            input = '(' + input.substring(0, 3) + ') ' + input.substring(3, 6);
        } else {
            input = '(' + input.substring(0, 3) + ') ' + input.substring(3, 6) + ' - ' + input.substring(6, 10);
        }
        $(this).val(input);
    });

    $(document).on('change', '#inquiry-form__client-role-1', function () {
        var $heading = $('#inquiry-form__step-2 .heading');
        var $addButton = $('#inquiry-form__step-2 #inquiry-form__add-another');
        var $step3Heading = $('#inquiry-form__step-3 .heading');
        var $step3ClientTypeLabel = $('#inquiry-form__step-3 .custom-form__field:first-child label');
        var $step3ClientTypeFirstOption = $('#inquiry-form__step-3 #inquiry-form__other-client-type-1 option:first-child');
        var $step3AddButton = $('#inquiry-form__step-3 #inquiry-form__other-add-another');
        var _value = this.value;
        var text = '';
        if ($heading.length) {
            $.each($heading, function (index, value) {
                text = $(value).text();
                if (_value === 'buyer') {
                    $(value).text(text.replace('Seller', 'Buyer'));
                } else if (_value === 'seller') {
                    $(value).text(text.replace('Buyer', 'Seller'));
                }
            });
        }
        if ($addButton.length) {
            $.each($addButton, function (index, value) {
                text = $(value).text();
                if (_value === 'buyer') {
                    $(value).text(text.replace('Seller', 'Buyer'));
                } else if (_value === 'seller') {
                    $(value).text(text.replace('Buyer', 'Seller'));
                }
            });
        }
        if ($step3Heading.length) {
            $.each($step3Heading, function(index, value) {
                text = $(value).text();
                if (_value === 'buyer') {
                    $(value).text(text.replace('Buyer', 'Seller'));
                } else if (_value === 'seller') {
                    $(value).text(text.replace('Seller', 'Buyer'));
                }
            });
        }
        if ($step3ClientTypeLabel.length) {
            text = $step3ClientTypeLabel.html();
            if (_value === 'buyer') {
                $step3ClientTypeLabel.html(text.replace('Buyer', 'Seller'));
            } else if (_value === 'seller') {
                $step3ClientTypeLabel.html(text.replace('Seller', 'Buyer'));
            }
        }
        if ($step3ClientTypeFirstOption.length) {
            text = $step3ClientTypeFirstOption.text();
            if (_value === 'buyer') {
                $step3ClientTypeFirstOption.text(text.replace('Buyer', 'Seller'));
            } else if (_value === 'seller') {
                $step3ClientTypeFirstOption.text(text.replace('Seller', 'Buyer'));
            }
        }
        if ($step3AddButton.length) {
            $.each($step3AddButton, function (index, value) {
                text = $(value).text();
                if (_value === 'buyer') {
                    $(value).text(text.replace('Seller', 'Buyer'));
                } else if (_value === 'seller') {
                    $(value).text(text.replace('Buyer', 'Seller'));
                }
            });
        }
    });

    $(document).on('keyup', '#inquiry-form__property-sales-price', function (event) {
        if (event.which >= 37 && event.which <= 40) {
            return;
        }
        $(this).val(function (index, value) {
            return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });

    $inquiryForm.submit(function (e) {
        e.preventDefault();

        var formErrors = [];
        var $formErrors = $inquiryForm.find('.custom-form__errors');
        var $formProgressBar = $inquiryForm.find('.custom-form__progress-bar');
        var $previousStep = $inquiryForm.find('#inquiry-form__previous-step');
        var $submit = $inquiryForm.find('.custom-form__submit');
        var propertyType = $inquiryForm.find('#inquiry-form__property-type option:selected').val();
        var propertyStreetAddress = $inquiryForm.find('#inquiry-form__property-street-address').val();
        var propertyCity = $inquiryForm.find('#inquiry-form__property-city').val();
        var propertyState = $inquiryForm.find('#inquiry-form__property-state option:selected').val();
        var propertyZip = $inquiryForm.find('#inquiry-form__property-zip').val();
        var propertySalesPrice = $inquiryForm.find('#inquiry-form__property-sales-price').val();
        var disclaimer = $inquiryForm.find('#inquiry-form__disclaimer').is(':checked');

        var ROW_INACTIVE_CLASS = 'custom-form__row--inactive';
        var BUTTON_INACTIVE_CLASS = 'custom-form__button--inactive';
        var SUBMIT_INACTIVE_CLASS = 'custom-form__submit--inactive';
        var formArray = $(this).serializeArray();

        if (!$formErrors.hasClass(ROW_INACTIVE_CLASS)) {
            $formErrors.addClass(ROW_INACTIVE_CLASS);
        }

        $("html, body").animate({ scrollTop: "0" });

        if (!propertyType.length) {
            formErrors.push('<li>Please enter the property type.</li>');
        }

        if (!propertyStreetAddress.length) {
            formErrors.push('<li>Please enter the property street address.</li>');
        }

        if (!propertyCity.length) {
            formErrors.push('<li>Please enter the property city.</li>');
        }

        if (!propertyState.length) {
            formErrors.push('<li>Please enter the property state.</li>');
        }

        if (!propertyZip.length) {
            formErrors.push('<li>Please enter the property zip.</li>');
        }

        if (propertyZip.length !== 5) {
            formErrors.push('<li>Please enter 5 numbers for the zip code for the property.</li>');
        }

        if (!propertySalesPrice.length) {
            formErrors.push('<li>Please enter the property property sales price.</li>');
        }

        if (!disclaimer) {
            formErrors.push('<li>Please confirm you have read the <a href="#disclaimer">disclaimer</a>.</li>');
        }

        if (formErrors.length) {
            $formErrors.find('ul').html(formErrors.join(''));
            $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
            return;
        }

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i].name] = formArray[i].value;
        }

        $('html, body').css('cursor', 'wait');
        $submit.prop('disabled', 'disabled');
        $submit.val('Submitting...');
        $formProgressBar.removeClass(ROW_INACTIVE_CLASS);
        $('#custom-form__progress').val(5);

        setTimeout(function() {
            $('#custom-form__progress').val(15);
        }, 1000);

        setTimeout(function() {
            $('#custom-form__progress').val(30);
        }, 2000);

        var $stepDisplay = $inquiryForm.find('#inquiry-form__step-display');
        $.ajax({
            method: 'POST',
            url: 'https://us-east4-bershteinlawreinquiryform.cloudfunctions.net/receiveFromSite',
            data: JSON.stringify(returnArray),
            contentType: 'application/json',
            beforeSend: function (xhr) {},
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    var percentComplete = Math.floor(e.loaded / e.total * 100);
                    $('#custom-form__progress').val(percentComplete);
                };
                return xhr;
            },
            success: function (response) {
                $('html, body').css('cursor', 'auto');
                $formProgressBar.addClass(ROW_INACTIVE_CLASS);
                $inquiryForm.find('#inquiry-form__step-4').toggleClass(ROW_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__step-4').hasClass(ROW_INACTIVE_CLASS));
                $stepDisplay.find('.heading').html('Inquiry Form Submitted');
                $stepDisplay.find('.custom-form__row-inner > div').html('Thank you for your submission.');
                $submit.val('Submit');
                if (!$previousStep.hasClass(BUTTON_INACTIVE_CLASS)) {
                    $previousStep.addClass(BUTTON_INACTIVE_CLASS);
                }
                if (!$submit.hasClass(SUBMIT_INACTIVE_CLASS)) {
                    $submit.addClass(SUBMIT_INACTIVE_CLASS);
                }
            },
            error: function (response) {
                $('html, body').css('cursor', 'auto');
                $formProgressBar.addClass(ROW_INACTIVE_CLASS);
                $formErrors.find('ul').html('<li>The submission has failed. Please wait a moment and try again. If the issue persists, please send an email to <a href="mailto:info@bershlaw.com">Info@BershLaw.com</a></li>');
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
                $submit.removeAttr('disabled');
                $submit.val('Submit');
            },
            fail: function (response) {
                $('html, body').css('cursor', 'auto');
                $formProgressBar.addClass(ROW_INACTIVE_CLASS);
                $formErrors.find('ul').html('<li>The submission has failed. Please wait a moment and try again. If the issue persists, please send an email to <a href="mailto:info@bershlaw.com">Info@BershLaw.com</a></li>');
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
                $submit.removeAttr('disabled');
                $submit.val('Submit');
            }
        });
    });
});
