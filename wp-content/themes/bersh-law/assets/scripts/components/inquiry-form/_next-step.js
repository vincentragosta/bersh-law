addAction(INIT, function () {
    var $inquiryForm = $('#inquiry-form');
    if (!$inquiryForm.length) {
        return;
    }

    var $previousStep = $inquiryForm.find('#inquiry-form__previous-step');
    var $formErrors = $inquiryForm.find('.custom-form__errors');
    var $nextStep = $inquiryForm.find('#inquiry-form__next-step');
    var $stepDisplay = $inquiryForm.find('#inquiry-form__step-display');
    var $stepsContainer = $inquiryForm.find('#inquiry-form__steps');
    var $stepOne = $inquiryForm.find('#inquiry-form__step-1');
    var $clientType = $inquiryForm.find('#inquiry-form__client-type-1');
    var $submit = $inquiryForm.find('.custom-form__submit');

    if (
        !$formErrors.length ||
        !$nextStep.length ||
        !$previousStep.length ||
        !$stepDisplay.length ||
        !$stepsContainer.length ||
        !$stepOne.length ||
        !$clientType.length ||
        !$submit.length
    ) {
        return;
    }

    var curatedClientRole = '';
    var ROW_INACTIVE_CLASS = 'custom-form__row--inactive';
    var BUTTON_INACTIVE_CLASS = 'custom-form__button--inactive';
    $nextStep.on('click', function () {
        var stepIndex = parseInt($inquiryForm.attr('data-step-index'));
        var formErrors = [];

        if (!$formErrors.hasClass(ROW_INACTIVE_CLASS)) {
            $formErrors.addClass(ROW_INACTIVE_CLASS);
        }

        $("html, body").animate({ scrollTop: "0" });

        var clientType = $clientType.find('option:selected').val();
        if (stepIndex === 1) {
            var clientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val();
            var clientRepresentation = $inquiryForm.find('#inquiry-form__client-representation-1 option:selected').val();

            if (!clientRole.length) {
                formErrors.push('<li>Please enter your client role.</li>');
            }

            if (!clientRepresentation.length) {
                formErrors.push('<li>Please enter whether or not we will represent you.</li>');
            }

            if (!clientType.length) {
                formErrors.push('<li>Please enter your client type.</li>');
            }

            if (!formErrors.length) {
                curatedClientRole = clientRole.substr(0, 1).toUpperCase() + clientRole.substr(1);
                $(this).text('Next Step: Add ' + (curatedClientRole === 'Buyer' ? 'Seller' : 'Buyer') + ' Information');
                $previousStep.text('Previous Step: Client Type');
                $previousStep.toggleClass(BUTTON_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__previous-step').hasClass(BUTTON_INACTIVE_CLASS));
                $stepOne.toggleClass(ROW_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__step-1').hasClass(ROW_INACTIVE_CLASS));
                $stepsContainer.find('#inquiry-form__step-2').toggleClass(ROW_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__step-2').hasClass(ROW_INACTIVE_CLASS));

                if (clientType === 'individual' && !$stepsContainer.find('#inquiry-form__step-2').length) {
                    $stepsContainer.append(
                        '<div class="custom-form__row-inner" id="inquiry-form__step-2" data-client-type="' + clientType + '">' +
                        '    <div class="custom-form__fields-container">' +
                        '        <div class="custom-form__fields-section">' +
                        '            <h2 class="heading heading--medium">' + curatedClientRole + ' 1</h2>' +
                        '            <div class="custom-form__fields">' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-first-name-1">Client First and Middle Name <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__client-first-name-1" name="inquiry-form__client-first-name-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-last-name-1">Client Last Name <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__client-last-name-1" name="inquiry-form__client-last-name-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-email-1">Client Email <span>*</span></label>' +
                        '                    <input type="email" id="inquiry-form__client-email-1" name="inquiry-form__client-email-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-phone-1">Client Phone <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__client-phone-1" name="inquiry-form__client-phone-1" />' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-street-address-1">Client Street Address <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__client-street-address-1"' +
                        '                           name="inquiry-form__client-street-address-1"/>' +
                        '                    <small>Please include apartment/unit/suite number</small>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-city-1">Client City <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__client-city-1" name="inquiry-form__client-city-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-state-1">Client State <span>*</span></label>' +
                        '                    <select id="inquiry-form__client-state-1" name="inquiry-form__client-state-1">' +
                        '                        <option value="">Select State</option>' +
                        '                        <option value="AL">Alabama</option>' +
                        '                        <option value="AK">Alaska</option>' +
                        '                        <option value="AZ">Arizona</option>' +
                        '                        <option value="AR">Arkansas</option>' +
                        '                        <option value="CA">California</option>' +
                        '                        <option value="CO">Colorado</option>' +
                        '                        <option value="CT">Connecticut</option>' +
                        '                        <option value="DE">Delaware</option>' +
                        '                        <option value="DC">District Of Columbia</option>' +
                        '                        <option value="FL">Florida</option>' +
                        '                        <option value="GA">Georgia</option>' +
                        '                        <option value="HI">Hawaii</option>' +
                        '                        <option value="ID">Idaho</option>' +
                        '                        <option value="IL">Illinois</option>' +
                        '                        <option value="IN">Indiana</option>' +
                        '                        <option value="IA">Iowa</option>' +
                        '                        <option value="KS">Kansas</option>' +
                        '                        <option value="KY">Kentucky</option>' +
                        '                        <option value="LA">Louisiana</option>' +
                        '                        <option value="ME">Maine</option>' +
                        '                        <option value="MD">Maryland</option>' +
                        '                        <option value="MA">Massachusetts</option>' +
                        '                        <option value="MI">Michigan</option>' +
                        '                        <option value="MN">Minnesota</option>' +
                        '                        <option value="MS">Mississippi</option>' +
                        '                        <option value="MO">Missouri</option>' +
                        '                        <option value="MT">Montana</option>' +
                        '                        <option value="NE">Nebraska</option>' +
                        '                        <option value="NV">Nevada</option>' +
                        '                        <option value="NH">New Hampshire</option>' +
                        '                        <option value="NJ">New Jersey</option>' +
                        '                        <option value="NM">New Mexico</option>' +
                        '                        <option value="NY">New York</option>' +
                        '                        <option value="NC">North Carolina</option>' +
                        '                        <option value="ND">North Dakota</option>' +
                        '                        <option value="OH">Ohio</option>' +
                        '                        <option value="OK">Oklahoma</option>' +
                        '                        <option value="OR">Oregon</option>' +
                        '                        <option value="PA">Pennsylvania</option>' +
                        '                        <option value="RI">Rhode Island</option>' +
                        '                        <option value="SC">South Carolina</option>' +
                        '                        <option value="SD">South Dakota</option>' +
                        '                        <option value="TN">Tennessee</option>' +
                        '                        <option value="TX">Texas</option>' +
                        '                        <option value="UT">Utah</option>' +
                        '                        <option value="VT">Vermont</option>' +
                        '                        <option value="VA">Virginia</option>' +
                        '                        <option value="WA">Washington</option>' +
                        '                        <option value="WV">West Virginia</option>' +
                        '                        <option value="WI">Wisconsin</option>' +
                        '                        <option value="WY">Wyoming</option>' +
                        '                    </select>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__client-zip-1">Client Zip <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__client-zip-1" name="inquiry-form__client-zip-1"/>' +
                        '                    <small>Please enter 5 digits</small>' +
                        '                </fieldset>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="custom-form__button-container">' +
                        '        <button type="button" id="inquiry-form__add-another" class="custom-form__button button button--inverted">+ Add Another ' + curatedClientRole + '</button>' +
                        '    </div>' +
                        '</div>'
                    );
                } else if (clientType === 'business-entity' && !$stepsContainer.find('#inquiry-form__step-2').length) {
                    $stepsContainer.append(
                        '<div class="custom-form__row-inner" id="inquiry-form__step-2" data-client-type="' + clientType + '">' +
                        '    <div class="custom-form__fields-container">' +
                        '        <div class="custom-form__fields-section">' +
                        '            <h2 class="heading heading--medium">' + curatedClientRole + ' 1</h2>' +
                        '            <div class="custom-form__fields">' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__business-entity-name-1">Business Entity Name <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__business-entity-name-1"' +
                        '                           name="inquiry-form__business-entity-name-1"/>' +
                        '                    <small>If the client is an estate please enter "Estate of [Decedent’s Name]" (e.g. Estate of John Smith).</small>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-first-name-1">Representative First and Middle Name <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__representative-first-name-1"' +
                        '                           name="inquiry-form__representative-first-name-1"/>' +
                        '                    <small>If the client is an estate, please include the Executor as the Representative.</small>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-last-name-1">Representative Last Name <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__representative-last-name-1"' +
                        '                           name="inquiry-form__representative-last-name-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-email-1">Representative Email <span>*</span></label>' +
                        '                    <input type="email" id="inquiry-form__representative-email-1"' +
                        '                           name="inquiry-form__representative-email-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-phone-1">Representative Phone <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__representative-phone-1"' +
                        '                           name="inquiry-form__representative-phone-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-street-address-1">Representative Street Address <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__representative-street-address-1"' +
                        '                           name="inquiry-form__representative-street-address-1"/>' +
                        '                    <small>Please include apartment/unit/suite number</small>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-city-1">Representative City <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__representative-city-1" name="inquiry-form__representative-city-1"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-state-1">Representative State <span>*</span></label>' +
                        '                    <select id="inquiry-form__representative-state-1" name="inquiry-form__representative-state-1">' +
                        '                        <option value="">Select State</option>' +
                        '                        <option value="AL">Alabama</option>' +
                        '                        <option value="AK">Alaska</option>' +
                        '                        <option value="AZ">Arizona</option>' +
                        '                        <option value="AR">Arkansas</option>' +
                        '                        <option value="CA">California</option>' +
                        '                        <option value="CO">Colorado</option>' +
                        '                        <option value="CT">Connecticut</option>' +
                        '                        <option value="DE">Delaware</option>' +
                        '                        <option value="DC">District Of Columbia</option>' +
                        '                        <option value="FL">Florida</option>' +
                        '                        <option value="GA">Georgia</option>' +
                        '                        <option value="HI">Hawaii</option>' +
                        '                        <option value="ID">Idaho</option>' +
                        '                        <option value="IL">Illinois</option>' +
                        '                        <option value="IN">Indiana</option>' +
                        '                        <option value="IA">Iowa</option>' +
                        '                        <option value="KS">Kansas</option>' +
                        '                        <option value="KY">Kentucky</option>' +
                        '                        <option value="LA">Louisiana</option>' +
                        '                        <option value="ME">Maine</option>' +
                        '                        <option value="MD">Maryland</option>' +
                        '                        <option value="MA">Massachusetts</option>' +
                        '                        <option value="MI">Michigan</option>' +
                        '                        <option value="MN">Minnesota</option>' +
                        '                        <option value="MS">Mississippi</option>' +
                        '                        <option value="MO">Missouri</option>' +
                        '                        <option value="MT">Montana</option>' +
                        '                        <option value="NE">Nebraska</option>' +
                        '                        <option value="NV">Nevada</option>' +
                        '                        <option value="NH">New Hampshire</option>' +
                        '                        <option value="NJ">New Jersey</option>' +
                        '                        <option value="NM">New Mexico</option>' +
                        '                        <option value="NY">New York</option>' +
                        '                        <option value="NC">North Carolina</option>' +
                        '                        <option value="ND">North Dakota</option>' +
                        '                        <option value="OH">Ohio</option>' +
                        '                        <option value="OK">Oklahoma</option>' +
                        '                        <option value="OR">Oregon</option>' +
                        '                        <option value="PA">Pennsylvania</option>' +
                        '                        <option value="RI">Rhode Island</option>' +
                        '                        <option value="SC">South Carolina</option>' +
                        '                        <option value="SD">South Dakota</option>' +
                        '                        <option value="TN">Tennessee</option>' +
                        '                        <option value="TX">Texas</option>' +
                        '                        <option value="UT">Utah</option>' +
                        '                        <option value="VT">Vermont</option>' +
                        '                        <option value="VA">Virginia</option>' +
                        '                        <option value="WA">Washington</option>' +
                        '                        <option value="WV">West Virginia</option>' +
                        '                        <option value="WI">Wisconsin</option>' +
                        '                        <option value="WY">Wyoming</option>' +
                        '                    </select>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__representative-zip-1">Representative Zip <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__representative-zip-1" name="inquiry-form__representative-zip-1"/>' +
                        '                    <small>Please enter 5 digits</small>' +
                        '                </fieldset>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="custom-form__button-container">' +
                        '        <button type="button" id="inquiry-form__add-another" class="custom-form__button button button--inverted">+ Add Another ' + curatedClientRole + '</button>' +
                        '    </div>' +
                        '</div>'
                    );
                }

                $inquiryForm.attr('data-step-index', stepIndex + 1);
                $stepDisplay.find('.heading > span').html(stepIndex + 1);
                $stepDisplay.find('.custom-form__row-inner > div').html('Please enter the client’s information below. If there are multiple client being represented, please include each as a separate entry. All fields are required.');
            } else {
                $("html, body").animate({ scrollTop: "0" });
                $formErrors.find('ul').html(formErrors.join(''));
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
            }
        } else if (stepIndex === 2) {
            var clientTypes = $inquiryForm.find('[id^=inquiry-form__client-type-]');
            var clientRepresentations = $inquiryForm.find('[id^=inquiry-form__client-representation-]');
            var clientFirstNames = $inquiryForm.find('[id^=inquiry-form__client-first-name-]');
            var clientLastNames = $inquiryForm.find('[id^=inquiry-form__client-last-name-]');
            var clientEmails = $inquiryForm.find('[id^=inquiry-form__client-email-]');
            var clientPhones = $inquiryForm.find('[id^=inquiry-form__client-phone-]');
            var clientStreetAddresses = $inquiryForm.find('[id^=inquiry-form__client-street-address-]');
            var clientCities = $inquiryForm.find('[id^=inquiry-form__client-city-]');
            var clientStates = $inquiryForm.find('[id^=inquiry-form__client-state-]');
            var clientZips = $inquiryForm.find('[id^=inquiry-form__client-zip-]');
            var businessEntityNames = $inquiryForm.find('[id^=inquiry-form__business-entity-name-]');
            var representativeFirstNames = $inquiryForm.find('[id^=inquiry-form__representative-first-name-]');
            var representativeLastNames = $inquiryForm.find('[id^=inquiry-form__representative-last-name-]');
            var representativeEmails = $inquiryForm.find('[id^=inquiry-form__representative-email-]');
            var representativePhones = $inquiryForm.find('[id^=inquiry-form__representative-phone-]');
            var representativeStreetAddresses = $inquiryForm.find('[id^=inquiry-form__representative-street-address-]');
            var representativeCities = $inquiryForm.find('[id^=inquiry-form__representative-city-]');
            var representativeStates = $inquiryForm.find('[id^=inquiry-form__representative-state-]');
            var representativeZips = $inquiryForm.find('[id^=inquiry-form__representative-zip-]');
            curatedClientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val() === 'buyer' ? 'Buyer' : 'Seller';

            $.each(clientTypes, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the client type for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientRepresentations, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the client representation for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientFirstNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the first and middle name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientLastNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the last name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientEmails, function (index, value) {
                var _value = $(value).val();
                if (!_value.match(/(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i)) {
                    formErrors.push('<li>Please enter a valid email address.</li>');
                }
                if (!_value.length) {
                    formErrors.push('<li>Please enter the email for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientPhones, function (index, value) {
                var _value = $(value).val();
                if (!_value.length) {
                    formErrors.push('<li>Please enter the phone for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
                if (_value.replace(/\D/g,'').length < 10) {
                    formErrors.push('<li>Please enter 10 numbers for the phone number of ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientStreetAddresses, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the street address for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientCities, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the city for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientStates, function (index, value) {
                if (!$(value).find('option:selected').val().length) {
                    formErrors.push('<li>Please enter the state for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(clientZips, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the zip for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
                if ($(value).val().length !== 5) {
                    formErrors.push('<li>Please enter 5 numbers for the zip code for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeFirstNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the last name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeLastNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the last name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(businessEntityNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the business entity name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeEmails, function (index, value) {
                var _value = $(value).val();
                if (!_value.match(/(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i)) {
                    formErrors.push('<li>Please enter a valid email address.</li>');
                }
                if (!_value.length) {
                    formErrors.push('<li>Please enter the email for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativePhones, function (index, value) {
                var _value = $(value).val();
                if (!_value.length) {
                    formErrors.push('<li>Please enter the phone for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
                if (_value.replace(/\D/g,'').length < 10) {
                    formErrors.push('<li>Please enter 10 numbers for the phone number of ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeStreetAddresses, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the street address for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeCities, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the city for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeStates, function (index, value) {
                if (!$(value).find('option:selected').val().length) {
                    formErrors.push('<li>Please enter the state for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(representativeZips, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the zip for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
                if ($(value).val().length !== 5) {
                    formErrors.push('<li>Please enter 5 numbers for the zip code for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            if (!formErrors.length) {
                $(this).text('Next Step: Add Property');
                $previousStep.text('Previous Step: Add ' + curatedClientRole + ' Information');
                var $stepTwo = $inquiryForm.find('#inquiry-form__step-2');
                $stepTwo.toggleClass(ROW_INACTIVE_CLASS, !$stepTwo.hasClass(ROW_INACTIVE_CLASS));
                $stepsContainer.find('#inquiry-form__step-3').toggleClass(ROW_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__step-3').hasClass(ROW_INACTIVE_CLASS));
                if (!$stepsContainer.find('#inquiry-form__step-3').length) {
                    $stepsContainer.append(
                        '<div class="custom-form__row-inner inquiry-form-step-3-inactive" id="inquiry-form__step-3">' +
                        '    <div class="custom-form__fields-container">' +
                        '        <div class="custom-form__fields-section">' +
                        '            <h2 class="heading heading--medium">' + (curatedClientRole === 'Buyer' ? 'Seller' : 'Buyer') + ' 1</h2>' +
                        '            <div class="custom-form__fields">' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__other-client-type-1">' + (curatedClientRole === 'Buyer' ? 'Seller' : 'Buyer') + ' Type <span>*</span></label>' +
                        '                    <select id="inquiry-form__other-client-type-1" name="inquiry-form__other-client-type-1">' +
                        '                        <option value="">Select ' + (curatedClientRole === 'Buyer' ? 'Seller' : 'Buyer') + ' Type</option>' +
                        '                        <option value="individual">Individual</option>' +
                        '                        <option value="business-entity">Business Entity</option>' +
                        '                    </select>' +
                        '                </fieldset>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="custom-form__button-container">' +
                        '        <button type="button" id="inquiry-form__other-add-another" class="custom-form__button button button--inverted">+ Add Another ' + (curatedClientRole === 'Buyer' ? 'Seller' : 'Buyer') + '</button>' +
                        '    </div>' +
                        '</div>'
                    );
                }
                $inquiryForm.attr('data-step-index', stepIndex + 1);
                $stepDisplay.find('.heading > span').html(stepIndex + 1);
                $stepDisplay.find('.custom-form__row-inner > div').html('Please enter the other side’s information. If there are multiple parties on the other side, please include each as a separate entity. All fields are required.');
            } else {
                $("html, body").animate({ scrollTop: "0" });
                $formErrors.find('ul').html(formErrors.join(''));
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
            }
        } else if (stepIndex === 3) {
            var otherClientTypes = $inquiryForm.find('[id^=inquiry-form__other-client-type-]');
            var otherClientRepresentations = $inquiryForm.find('[id^=inquiry-form__other-client-representation-]');
            var otherClientFirstNames = $inquiryForm.find('[id^=inquiry-form__other-client-first-name-]');
            var otherClientLastNames = $inquiryForm.find('[id^=inquiry-form__other-client-last-name-]');
            // var otherClientEmails = $inquiryForm.find('[id^=inquiry-form__other-client-email-]');
            // var otherClientPhones = $inquiryForm.find('[id^=inquiry-form__other-client-phone-]');
            var otherClientStreetAddresses = $inquiryForm.find('[id^=inquiry-form__other-client-street-address-]');
            var otherClientCities = $inquiryForm.find('[id^=inquiry-form__other-client-city-]');
            var otherClientStates = $inquiryForm.find('[id^=inquiry-form__other-client-state-]');
            var otherClientZips = $inquiryForm.find('[id^=inquiry-form__other-client-zip-]');
            var otherBusinessEntityNames = $inquiryForm.find('[id^=inquiry-form__other-business-entity-name-]');
            var otherRepresentativeFirstNames = $inquiryForm.find('[id^=inquiry-form__other-representative-first-name-]');
            var otherRepresentativeLastNames = $inquiryForm.find('[id^=inquiry-form__other-representative-last-name-]');
            // var otherRepresentativeEmails = $inquiryForm.find('[id^=inquiry-form__other-representative-email-]');
            // var otherRepresentativePhones = $inquiryForm.find('[id^=inquiry-form__other-representative-phone-]');
            var otherRepresentativeStreetAddresses = $inquiryForm.find('[id^=inquiry-form__other-representative-street-address-]');
            var otherRepresentativeCities = $inquiryForm.find('[id^=inquiry-form__other-representative-city-]');
            var otherRepresentativeStates = $inquiryForm.find('[id^=inquiry-form__other-representative-state-]');
            var otherRepresentativeZips = $inquiryForm.find('[id^=inquiry-form__other-representative-zip-]');
            curatedClientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val() === 'buyer' ? 'Seller' : 'Buyer';

            $.each(otherClientTypes, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the client type for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherClientRepresentations, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the client representation for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherClientFirstNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the first and middle name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherClientLastNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the last name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            // $.each(otherClientEmails, function (index, value) {
            //     var _value = $(value).val();
            //     if (!_value.match(/(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i)) {
            //         formErrors.push('<li>Please enter a valid email address.</li>');
            //     }
            //     if (!_value.length) {
            //         formErrors.push('<li>Please enter the email for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
            //     }
            // });
            //
            // $.each(otherClientPhones, function (index, value) {
            //     var _value = $(value).val();
            //     if (!_value.length) {
            //         formErrors.push('<li>Please enter the phone for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
            //     }
            //     if (_value.replace(/\D/g,'').length < 10) {
            //         formErrors.push('<li>Please enter 10 numbers for the phone number of ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
            //     }
            // });

            $.each(otherClientStreetAddresses, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the street address for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherClientCities, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the city for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherClientStates, function (index, value) {
                if (!$(value).find('option:selected').val().length) {
                    formErrors.push('<li>Please enter the state for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherClientZips, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the zip for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
                if ($(value).val().length !== 5) {
                    formErrors.push('<li>Please enter 5 numbers for the zip code for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherRepresentativeFirstNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the last name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherRepresentativeLastNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the last name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherBusinessEntityNames, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the business entity name for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            // $.each(otherRepresentativeEmails, function (index, value) {
            //     var _value = $(value).val();
            //     if (!_value.match(/(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i)) {
            //         formErrors.push('<li>Please enter a valid email address.</li>');
            //     }
            //     if (!_value.length) {
            //         formErrors.push('<li>Please enter the email for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
            //     }
            // });
            //
            // $.each(otherRepresentativePhones, function (index, value) {
            //     var _value = $(value).val();
            //     if (!_value.length) {
            //         formErrors.push('<li>Please enter the phone for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
            //     }
            //     if (_value.replace(/\D/g,'').length < 10) {
            //         formErrors.push('<li>Please enter 10 numbers for the phone number of ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
            //     }
            // });

            $.each(otherRepresentativeStreetAddresses, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the street address for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherRepresentativeCities, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the city for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherRepresentativeStates, function (index, value) {
                if (!$(value).find('option:selected').val().length) {
                    formErrors.push('<li>Please enter the state for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            $.each(otherRepresentativeZips, function (index, value) {
                if (!$(value).val().length) {
                    formErrors.push('<li>Please enter the zip for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
                if ($(value).val().length !== 5) {
                    formErrors.push('<li>Please enter 5 numbers for the zip code for ' + curatedClientRole + ' ' + (index + 1) + '.</li>');
                }
            });

            if (!formErrors.length) {
                if (!$(this).hasClass(BUTTON_INACTIVE_CLASS)) {
                    $(this).addClass(BUTTON_INACTIVE_CLASS);
                }
                $previousStep.text('Previous Step: Add ' + curatedClientRole + ' Information');
                $inquiryForm.find('#inquiry-form__step-3').toggleClass(ROW_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__step-3').hasClass(ROW_INACTIVE_CLASS));
                $inquiryForm.find('#inquiry-form__step-4').toggleClass(ROW_INACTIVE_CLASS, !$inquiryForm.find('#inquiry-form__step-4').hasClass(ROW_INACTIVE_CLASS));
                if (!$stepsContainer.find('#inquiry-form__step-4').length) {
                    $stepsContainer.append(
                        '<div class="custom-form__row-inner" id="inquiry-form__step-4">' +
                        '    <div class="custom-form__fields-container">' +
                        '        <div class="custom-form__fields-section">' +
                        '            <h2 class="heading heading--medium">Property</h2>' +
                        '            <div class="custom-form__fields">' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__property-type">Property Type <span>*</span></label>' +
                        '                    <select id="inquiry-form__property-type" name="inquiry-form__property-type">' +
                        '                        <option value="">Select Property Type</option>' +
                        '                        <option value="standalone">Standalone</option>' +
                        '                        <option value="condominium">Condominium</option>' +
                        '                        <option value="homeowners-association">Homeowner\'s Association</option>' +
                        '                        <option value="co-op">Co-op</option>' +
                        '                    </select>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__property-street-address">Property Street Address <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__property-street-address"' +
                        '                           name="inquiry-form__property-street-address"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__property-city">Property City <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__property-city" name="inquiry-form__property-city"/>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__property-state">Property State <span>*</span></label>' +
                        '                    <select id="inquiry-form__property-state" name="inquiry-form__property-state">' +
                        '                        <option value="">Select State</option>' +
                        '                        <option value="AL">Alabama</option>' +
                        '                        <option value="AK">Alaska</option>' +
                        '                        <option value="AZ">Arizona</option>' +
                        '                        <option value="AR">Arkansas</option>' +
                        '                        <option value="CA">California</option>' +
                        '                        <option value="CO">Colorado</option>' +
                        '                        <option value="CT">Connecticut</option>' +
                        '                        <option value="DE">Delaware</option>' +
                        '                        <option value="DC">District Of Columbia</option>' +
                        '                        <option value="FL">Florida</option>' +
                        '                        <option value="GA">Georgia</option>' +
                        '                        <option value="HI">Hawaii</option>' +
                        '                        <option value="ID">Idaho</option>' +
                        '                        <option value="IL">Illinois</option>' +
                        '                        <option value="IN">Indiana</option>' +
                        '                        <option value="IA">Iowa</option>' +
                        '                        <option value="KS">Kansas</option>' +
                        '                        <option value="KY">Kentucky</option>' +
                        '                        <option value="LA">Louisiana</option>' +
                        '                        <option value="ME">Maine</option>' +
                        '                        <option value="MD">Maryland</option>' +
                        '                        <option value="MA">Massachusetts</option>' +
                        '                        <option value="MI">Michigan</option>' +
                        '                        <option value="MN">Minnesota</option>' +
                        '                        <option value="MS">Mississippi</option>' +
                        '                        <option value="MO">Missouri</option>' +
                        '                        <option value="MT">Montana</option>' +
                        '                        <option value="NE">Nebraska</option>' +
                        '                        <option value="NV">Nevada</option>' +
                        '                        <option value="NH">New Hampshire</option>' +
                        '                        <option value="NJ">New Jersey</option>' +
                        '                        <option value="NM">New Mexico</option>' +
                        '                        <option value="NY">New York</option>' +
                        '                        <option value="NC">North Carolina</option>' +
                        '                        <option value="ND">North Dakota</option>' +
                        '                        <option value="OH">Ohio</option>' +
                        '                        <option value="OK">Oklahoma</option>' +
                        '                        <option value="OR">Oregon</option>' +
                        '                        <option value="PA">Pennsylvania</option>' +
                        '                        <option value="RI">Rhode Island</option>' +
                        '                        <option value="SC">South Carolina</option>' +
                        '                        <option value="SD">South Dakota</option>' +
                        '                        <option value="TN">Tennessee</option>' +
                        '                        <option value="TX">Texas</option>' +
                        '                        <option value="UT">Utah</option>' +
                        '                        <option value="VT">Vermont</option>' +
                        '                        <option value="VA">Virginia</option>' +
                        '                        <option value="WA">Washington</option>' +
                        '                        <option value="WV">West Virginia</option>' +
                        '                        <option value="WI">Wisconsin</option>' +
                        '                        <option value="WY">Wyoming</option>' +
                        '                    </select>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__property-zip">Property Zip <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__property-zip" name="inquiry-form__property-zip"/>' +
                        '                    <small>Please enter 5 digits</small>' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field">' +
                        '                    <label for="inquiry-form__property-sales-price">Property Sales Price <span>*</span></label>' +
                        '                    <input type="text" id="inquiry-form__property-sales-price" name="inquiry-form__property-sales-price" />' +
                        '                </fieldset>' +
                        '                <fieldset class="custom-form__field custom-form__field--checkbox">' +
                        '                    <label for="inquiry-form__disclaimer"><input id="inquiry-form__disclaimer" name="inquiry-form__disclaimer" type="checkbox"><p>I have read the <a href="#disclaimer">Disclaimer</a>. <span>*</span></p></label>' +
                        '                </fieldset>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '</div>'
                    );
                }
                var SUBMIT_INACTIVE = 'custom-form__submit--inactive';
                $inquiryForm.attr('data-step-index', stepIndex + 1);
                $stepDisplay.find('.heading > span').html(stepIndex + 1);
                $stepDisplay.find('.custom-form__row-inner > div').html('Please enter the property information. All fields are required.');
                $submit.toggleClass(SUBMIT_INACTIVE, !$submit.hasClass(SUBMIT_INACTIVE));
            } else {
                $("html, body").animate({ scrollTop: "0" });
                $formErrors.find('ul').html(formErrors.join(''));
                $formErrors.toggleClass(ROW_INACTIVE_CLASS, !formErrors.length && !$formErrors.hasClass(ROW_INACTIVE_CLASS));
            }
        }
    });
});
