addAction(INIT, function () {
    var $inquiryForm = $('#inquiry-form');
    if (!$inquiryForm.length) {
        return;
    }

    var $stepsContainer = $inquiryForm.find('#inquiry-form__steps');
    if (!$stepsContainer.length) {
        return;
    }

    var individualCounter = 1;
    $(document).on('click', '#inquiry-form__add-another', function (e) {
        e.preventDefault();
        if (individualCounter++ < 10) {
            var clientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val();
            var curatedClientRole = clientRole.substr(0, 1).toUpperCase() + clientRole.substr(1);
            $stepsContainer.find('#inquiry-form__step-2 .custom-form__fields-container').append(
                '<div class="custom-form__fields-section">' +
                '    <div class="custom-form__heading-container">' +
                '        <h2 class="heading heading--medium">' + curatedClientRole + ' ' + individualCounter + '</h2>' +
                '        <div class="custom-form__button-container">' +
                '            <button type="button" class="custom-form__copy custom-form__copy--inactive button button--inverted">Copy From ' + curatedClientRole + ' 1</button>' +
                '            <button type="button" class="custom-form__remove button button--inverted">Remove ' + curatedClientRole + ' ' + individualCounter + '</button>' +
                '        </div>' +
                '    </div>' +
                '    <div class="custom-form__fields">' +
                '        <fieldset class="custom-form__field">' +
                '            <label for="inquiry-form__client-type-' + individualCounter + '">Client Type <span>*</span></label>' +
                '            <select id="inquiry-form__client-type-' + individualCounter + '" name="inquiry-form__client-type-' + individualCounter + '">' +
                '                <option value="">Select Client Type</option>' +
                '                <option value="individual">Individual</option>' +
                '                <option value="business-entity">Business Entity</option>' +
                '            </select>' +
                '            <input type="hidden" value="' + clientRole + '" id="inquiry-form__client-role-' + individualCounter + '" name="inquiry-form__client-role-' + individualCounter + '" />' +
                '        </fieldset>' +
                '        <fieldset class="custom-form__field">' +
                '            <label for="inquiry-form__client-representation-' + individualCounter + '">Will Bershtein Law Represent this Client? <span>*</span></label>' +
                '            <select id="inquiry-form__client-representation-' + individualCounter + '" name="inquiry-form__client-representation-' + individualCounter + '">' +
                '                <option value="">Select Representation</option>' +
                '                <option value="yes">Yes</option>' +
                '                <option value="no">No</option>' +
                '            </select>' +
                '        </fieldset>' +
                '    </div>' +
                '</div>'
            );
        }
    });

    $(document).on('click', '.custom-form__copy', function () {
        var $sectionFormFields = $(this).parents('.custom-form__fields-section').find('.custom-form__field');
        $.each($sectionFormFields, function (index, value) {
            var id, identifier, _value = '';
            if ($(value).find('input:not([id^=inquiry-form__client-first-name-]):not([id^=inquiry-form__client-last-name-]):not([id^=inquiry-form__business-entity-name-]):not([id^=inquiry-form__client-email-]):not([id^=inquiry-form__client-phone-]):not([id^=inquiry-form__representative-first-name-]):not([id^=inquiry-form__representative-last-name-]):not([id^=inquiry-form__representative-email-]):not([id^=inquiry-form__representative-phone-])').length) {
                id = $(value).find('input').attr('id');
                identifier = id.substring(0, id.length - 1);
                _value = $inquiryForm.find('#' + identifier + '1').val();
                if (_value) {
                    document.getElementById(id).value = _value;
                }
            }
            if ($(value).find('select:not([id^=inquiry-form__client-type-])').length) {
                id = $(value).find('select').attr('id');
                identifier = id.substring(0, id.length - 1);
                _value = $inquiryForm.find('#' + identifier + '1 option:selected').val();
                document.getElementById(id).value = _value;
                if (_value) {
                    document.getElementById(id).value = _value;
                }
            }
        });
    });

    $(document).on('change', '#inquiry-form__client-type-2, #inquiry-form__client-type-3, #inquiry-form__client-type-4, #inquiry-form__client-type-5, #inquiry-form__client-type-6, #inquiry-form__client-type-7, #inquiry-form__client-type-8, #inquiry-form__client-type-9, #inquiry-form__client-type-10', function () {
        if ($(this).parent('.custom-form__field').siblings().length > 1) {
            var $fields = $(this).parents('.custom-form__fields').find('.custom-form__field:nth-child(n+3)');
            if ($fields.length) {
                $.each($fields, function (index, value) {
                    $(value).remove();
                });
            }
        }
        if ($inquiryForm.find('#inquiry-form__client-type-1 option:selected').val() === this.value && $inquiryForm.find('.custom-form__copy').hasClass('custom-form__copy--inactive')) {
            $inquiryForm.find('.custom-form__copy').removeClass('custom-form__copy--inactive');
        } else if ($inquiryForm.find('#inquiry-form__client-type-1 option:selected').val() !== this.value) {
            $inquiryForm.find('.custom-form__copy').addClass('custom-form__copy--inactive');
        }
        if (this.value === 'individual') {
            $stepsContainer.find('#inquiry-form__step-2 .custom-form__fields-section:last-child  .custom-form__fields').append(
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-first-name-' + individualCounter + '">Client First and Middle Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__client-first-name-' + individualCounter + '" name="inquiry-form__client-first-name-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-last-name-' + individualCounter + '">Client Last Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__client-last-name-' + individualCounter + '" name="inquiry-form__client-last-name-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-email-' + individualCounter + '">Client Email <span>*</span></label>' +
                '    <input type="email" id="inquiry-form__client-email-' + individualCounter + '" name="inquiry-form__client-email-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-phone-' + individualCounter + '">Client Phone <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__client-phone-' + individualCounter + '" name="inquiry-form__client-phone-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-street-address-' + individualCounter + '">Client Street Address <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__client-street-address-' + individualCounter + '" name="inquiry-form__client-street-address-' + individualCounter + '" />' +
                '    <small>Please include apartment/unit/suite number</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-city-' + individualCounter + '">Client City <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__client-city-' + individualCounter + '" name="inquiry-form__client-city-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-state-' + individualCounter + '">Client State <span>*</span></label>' +
                '    <select id="inquiry-form__client-state-' + individualCounter + '" name="inquiry-form__client-state-' + individualCounter + '">' +
                '        <option value="">Select State</option>' +
                '        <option value="AL">Alabama</option>' +
                '        <option value="AK">Alaska</option>' +
                '        <option value="AZ">Arizona</option>' +
                '        <option value="AR">Arkansas</option>' +
                '        <option value="CA">California</option>' +
                '        <option value="CO">Colorado</option>' +
                '        <option value="CT">Connecticut</option>' +
                '        <option value="DE">Delaware</option>' +
                '        <option value="DC">District Of Columbia</option>' +
                '        <option value="FL">Florida</option>' +
                '        <option value="GA">Georgia</option>' +
                '        <option value="HI">Hawaii</option>' +
                '        <option value="ID">Idaho</option>' +
                '        <option value="IL">Illinois</option>' +
                '        <option value="IN">Indiana</option>' +
                '        <option value="IA">Iowa</option>' +
                '        <option value="KS">Kansas</option>' +
                '        <option value="KY">Kentucky</option>' +
                '        <option value="LA">Louisiana</option>' +
                '        <option value="ME">Maine</option>' +
                '        <option value="MD">Maryland</option>' +
                '        <option value="MA">Massachusetts</option>' +
                '        <option value="MI">Michigan</option>' +
                '        <option value="MN">Minnesota</option>' +
                '        <option value="MS">Mississippi</option>' +
                '        <option value="MO">Missouri</option>' +
                '        <option value="MT">Montana</option>' +
                '        <option value="NE">Nebraska</option>' +
                '        <option value="NV">Nevada</option>' +
                '        <option value="NH">New Hampshire</option>' +
                '        <option value="NJ">New Jersey</option>' +
                '        <option value="NM">New Mexico</option>' +
                '        <option value="NY">New York</option>' +
                '        <option value="NC">North Carolina</option>' +
                '        <option value="ND">North Dakota</option>' +
                '        <option value="OH">Ohio</option>' +
                '        <option value="OK">Oklahoma</option>' +
                '        <option value="OR">Oregon</option>' +
                '        <option value="PA">Pennsylvania</option>' +
                '        <option value="RI">Rhode Island</option>' +
                '        <option value="SC">South Carolina</option>' +
                '        <option value="SD">South Dakota</option>' +
                '        <option value="TN">Tennessee</option>' +
                '        <option value="TX">Texas</option>' +
                '        <option value="UT">Utah</option>' +
                '        <option value="VT">Vermont</option>' +
                '        <option value="VA">Virginia</option>' +
                '        <option value="WA">Washington</option>' +
                '        <option value="WV">West Virginia</option>' +
                '        <option value="WI">Wisconsin</option>' +
                '        <option value="WY">Wyoming</option>' +
                '    </select>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__client-zip-' + individualCounter + '">Client Zip <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__client-zip-' + individualCounter + '" name="inquiry-form__client-zip-' + individualCounter + '" />' +
                '    <small>Please enter 5 digits</small>' +
                '</fieldset>'
            );
        } else if (this.value === 'business-entity') {
            $stepsContainer.find('#inquiry-form__step-2 .custom-form__fields-section:last-child .custom-form__fields').append(
                '<fieldset class="custom-form__field">' +
                '      <label for="inquiry-form__business-entity-name-' + individualCounter + '">Business Entity Name <span>*</span></label>' +
                '      <input type="text" id="inquiry-form__business-entity-name-' + individualCounter + '" name="inquiry-form__business-entity-name-' + individualCounter + '" />' +
                '      <small>If the client is an estate please enter "Estate of [Decedent’s Name]" (e.g. Estate of John Smith).</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-first-name-' + individualCounter + '">Representative First and Middle Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__representative-first-name-' + individualCounter + '" name="inquiry-form__representative-first-name-' + individualCounter + '" />' +
                '    <small>If the client is an estate, please include the Executor as the Representative.</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-last-name-' + individualCounter + '">Representative Last Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__representative-last-name-' + individualCounter + '" name="inquiry-form__representative-last-name-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-email-' + individualCounter + '">Representative Email <span>*</span></label>' +
                '    <input type="email" id="inquiry-form__representative-email-' + individualCounter + '" name="inquiry-form__representative-email-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-phone-' + individualCounter + '">Representative Phone <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__representative-phone-' + individualCounter + '" name="inquiry-form__representative-phone-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-street-address-' + individualCounter + '">Representative Street Address <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__representative-street-address-' + individualCounter + '" name="inquiry-form__representative-street-address-' + individualCounter + '" />' +
                '    <small>Please include apartment/unit/suite number</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-city-' + individualCounter + '">Representative City <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__representative-city-' + individualCounter + '" name="inquiry-form__representative-city-' + individualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-state-' + individualCounter + '">Representative State <span>*</span></label>' +
                '    <select id="inquiry-form__representative-state-' + individualCounter + '" name="inquiry-form__representative-state-' + individualCounter + '">' +
                '          <option value="">Select State</option>' +
                '          <option value="AL">Alabama</option>' +
                '          <option value="AK">Alaska</option>' +
                '          <option value="AZ">Arizona</option>' +
                '          <option value="AR">Arkansas</option>' +
                '          <option value="CA">California</option>' +
                '          <option value="CO">Colorado</option>' +
                '          <option value="CT">Connecticut</option>' +
                '          <option value="DE">Delaware</option>' +
                '          <option value="DC">District Of Columbia</option>' +
                '          <option value="FL">Florida</option>' +
                '          <option value="GA">Georgia</option>' +
                '          <option value="HI">Hawaii</option>' +
                '          <option value="ID">Idaho</option>' +
                '          <option value="IL">Illinois</option>' +
                '          <option value="IN">Indiana</option>' +
                '          <option value="IA">Iowa</option>' +
                '          <option value="KS">Kansas</option>' +
                '          <option value="KY">Kentucky</option>' +
                '          <option value="LA">Louisiana</option>' +
                '          <option value="ME">Maine</option>' +
                '          <option value="MD">Maryland</option>' +
                '          <option value="MA">Massachusetts</option>' +
                '          <option value="MI">Michigan</option>' +
                '          <option value="MN">Minnesota</option>' +
                '          <option value="MS">Mississippi</option>' +
                '          <option value="MO">Missouri</option>' +
                '          <option value="MT">Montana</option>' +
                '          <option value="NE">Nebraska</option>' +
                '          <option value="NV">Nevada</option>' +
                '          <option value="NH">New Hampshire</option>' +
                '          <option value="NJ">New Jersey</option>' +
                '          <option value="NM">New Mexico</option>' +
                '          <option value="NY">New York</option>' +
                '          <option value="NC">North Carolina</option>' +
                '          <option value="ND">North Dakota</option>' +
                '          <option value="OH">Ohio</option>' +
                '          <option value="OK">Oklahoma</option>' +
                '          <option value="OR">Oregon</option>' +
                '          <option value="PA">Pennsylvania</option>' +
                '          <option value="RI">Rhode Island</option>' +
                '          <option value="SC">South Carolina</option>' +
                '          <option value="SD">South Dakota</option>' +
                '          <option value="TN">Tennessee</option>' +
                '          <option value="TX">Texas</option>' +
                '          <option value="UT">Utah</option>' +
                '          <option value="VT">Vermont</option>' +
                '          <option value="VA">Virginia</option>' +
                '          <option value="WA">Washington</option>' +
                '          <option value="WV">West Virginia</option>' +
                '          <option value="WI">Wisconsin</option>' +
                '          <option value="WY">Wyoming</option>' +
                '    </select>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__representative-zip-' + individualCounter + '">Representative Zip <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__representative-zip-' + individualCounter + '" name="inquiry-form__representative-zip-' + individualCounter + '" />' +
                '    <small>Please enter 5 digits</small>' +
                '</fieldset>'
            );
        }
    });

    $(document).on('click', '.custom-form__remove', function (e) {
        e.preventDefault();
        var $section = $(this).parents('.custom-form__fields-section');

        if ($section.length) {
            $section.remove();
            individualCounter--;
        }
    });
});
