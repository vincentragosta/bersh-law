addAction(INIT, function () {
    var $inquiryForm = $('#inquiry-form');
    if (!$inquiryForm.length) {
        return;
    }

    var $stepsContainer = $inquiryForm.find('#inquiry-form__steps');
    if (!$stepsContainer.length) {
        return;
    }

    var otherIndividualCounter = 1;
    $(document).on('click', '#inquiry-form__other-add-another', function (e) {
        e.preventDefault();
        if (otherIndividualCounter++ < 10) {
            var clientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val();
            var curatedClientRole = clientRole === 'buyer' ? 'Seller' : 'Buyer';
            $stepsContainer.find('#inquiry-form__step-3 .custom-form__fields-container').append(
                '<div class="custom-form__fields-section">' +
                '    <div class="custom-form__heading-container">' +
                '        <h2 class="heading heading--medium">' + curatedClientRole + ' ' + otherIndividualCounter + '</h2>' +
                '        <div class="custom-form__button-container">' +
                '            <button type="button" class="custom-form__other-copy custom-form__other-copy--inactive button button--inverted">Copy From ' + curatedClientRole + ' 1</button>' +
                '            <button type="button" class="custom-form__other-remove button button--inverted">Remove ' + curatedClientRole + ' ' + otherIndividualCounter + '</button>' +
                '        </div>' +
                '    </div>' +
                '    <div class="custom-form__fields">' +
                '        <fieldset class="custom-form__field">' +
                '            <label for="inquiry-form__other-client-type-' + otherIndividualCounter + '">' + curatedClientRole + ' Type <span>*</span></label>' +
                '            <select id="inquiry-form__other-client-type-' + otherIndividualCounter + '" name="inquiry-form__other-client-type-' + otherIndividualCounter + '">' +
                '                <option value="">Select ' + curatedClientRole + ' Type</option>' +
                '                <option value="individual">Individual</option>' +
                '                <option value="business-entity">Business Entity</option>' +
                '            </select>' +
                '            <input type="hidden" value="' + clientRole + '" id="inquiry-form__other-client-role-' + otherIndividualCounter + '" name="inquiry-form__other-client-role-' + otherIndividualCounter + '" />' +
                '        </fieldset>' +
                '    </div>' +
                '</div>'
            );
        }
    });

    $(document).on('click', '.custom-form__other-copy', function () {
        var $sectionFormFields = $(this).parents('.custom-form__fields-section').find('.custom-form__field');
        $.each($sectionFormFields, function (index, value) {
            var id, identifier, _value = '';
            if ($(value).find('input:not([id^=inquiry-form__other-client-first-name-]):not([id^=inquiry-form__other-client-last-name-]):not([id^=inquiry-form__other-business-entity-name-]):not([id^=inquiry-form__other-client-email-]):not([id^=inquiry-form__other-client-phone-]):not([id^=inquiry-form__other-representative-first-name-]):not([id^=inquiry-form__other-representative-last-name-]):not([id^=inquiry-form__other-representative-email-]):not([id^=inquiry-form__other-representative-phone-])').length) {
                id = $(value).find('input').attr('id');
                identifier = id.substring(0, id.length - 1);
                _value = $inquiryForm.find('#' + identifier + '1').val();
                if (_value) {
                    document.getElementById(id).value = _value;
                }
            }
            if ($(value).find('select').length) {
                id = $(value).find('select').attr('id');
                identifier = id.substring(0, id.length - 1);
                _value = $inquiryForm.find('#' + identifier + '1 option:selected').val();
                document.getElementById(id).value = _value;
                if (_value) {
                    document.getElementById(id).value = _value;
                }
            }
        });
    });

    $(document).on('change', '#inquiry-form__other-client-type-1', function() {
        var CUSTOM_STEP_3_CLASS = 'inquiry-form-step-3-inactive';
        var $stepThree = $inquiryForm.find('#inquiry-form__step-3');
        var clientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val();
        var curatedClientRole = clientRole === 'buyer' ? 'Seller' : 'Buyer';
        if ($stepThree.hasClass(CUSTOM_STEP_3_CLASS)) {
            $stepThree.removeClass(CUSTOM_STEP_3_CLASS);
        }

        if ($(this).parent('.custom-form__field').siblings().length > 1) {
            var $fields = $(this).parents('.custom-form__fields').find('.custom-form__field:nth-child(n+2)');
            if ($fields.length) {
                $.each($fields, function(index, value) {
                    $(value).remove();
                });
            }
        }

        if (this.value === 'individual') {
            $stepsContainer.find('#inquiry-form__step-3 .custom-form__fields-section .custom-form__fields').append(
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-first-name-1">' + curatedClientRole + ' First and Middle Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-first-name-1" name="inquiry-form__other-client-first-name-1" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-last-name-1">' + curatedClientRole + ' Last Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-last-name-1" name="inquiry-form__other-client-last-name-1" />' +
                '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-client-email-1">' + curatedClientRole + ' Email <span>*</span></label>' +
                // '    <input type="email" id="inquiry-form__other-client-email-1" name="inquiry-form__other-client-email-1" />' +
                // '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-client-phone-1">' + curatedClientRole + ' Phone <span>*</span></label>' +
                // '    <input type="text" id="inquiry-form__other-client-phone-1" name="inquiry-form__other-client-phone-d1" />' +
                // '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-street-address-1">' + curatedClientRole + ' Street Address <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-street-address-1" name="inquiry-form__other-client-street-address-1" />' +
                '    <small>Please include apartment/unit/suite number</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-city-1">' + curatedClientRole + ' City <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-city-1" name="inquiry-form__other-client-city-1" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-state-1">' + curatedClientRole + ' State <span>*</span></label>' +
                '    <select id="inquiry-form__other-client-state-1" name="inquiry-form__other-client-state-1">' +
                '        <option value="">Select State</option>' +
                '        <option value="AL">Alabama</option>' +
                '        <option value="AK">Alaska</option>' +
                '        <option value="AZ">Arizona</option>' +
                '        <option value="AR">Arkansas</option>' +
                '        <option value="CA">California</option>' +
                '        <option value="CO">Colorado</option>' +
                '        <option value="CT">Connecticut</option>' +
                '        <option value="DE">Delaware</option>' +
                '        <option value="DC">District Of Columbia</option>' +
                '        <option value="FL">Florida</option>' +
                '        <option value="GA">Georgia</option>' +
                '        <option value="HI">Hawaii</option>' +
                '        <option value="ID">Idaho</option>' +
                '        <option value="IL">Illinois</option>' +
                '        <option value="IN">Indiana</option>' +
                '        <option value="IA">Iowa</option>' +
                '        <option value="KS">Kansas</option>' +
                '        <option value="KY">Kentucky</option>' +
                '        <option value="LA">Louisiana</option>' +
                '        <option value="ME">Maine</option>' +
                '        <option value="MD">Maryland</option>' +
                '        <option value="MA">Massachusetts</option>' +
                '        <option value="MI">Michigan</option>' +
                '        <option value="MN">Minnesota</option>' +
                '        <option value="MS">Mississippi</option>' +
                '        <option value="MO">Missouri</option>' +
                '        <option value="MT">Montana</option>' +
                '        <option value="NE">Nebraska</option>' +
                '        <option value="NV">Nevada</option>' +
                '        <option value="NH">New Hampshire</option>' +
                '        <option value="NJ">New Jersey</option>' +
                '        <option value="NM">New Mexico</option>' +
                '        <option value="NY">New York</option>' +
                '        <option value="NC">North Carolina</option>' +
                '        <option value="ND">North Dakota</option>' +
                '        <option value="OH">Ohio</option>' +
                '        <option value="OK">Oklahoma</option>' +
                '        <option value="OR">Oregon</option>' +
                '        <option value="PA">Pennsylvania</option>' +
                '        <option value="RI">Rhode Island</option>' +
                '        <option value="SC">South Carolina</option>' +
                '        <option value="SD">South Dakota</option>' +
                '        <option value="TN">Tennessee</option>' +
                '        <option value="TX">Texas</option>' +
                '        <option value="UT">Utah</option>' +
                '        <option value="VT">Vermont</option>' +
                '        <option value="VA">Virginia</option>' +
                '        <option value="WA">Washington</option>' +
                '        <option value="WV">West Virginia</option>' +
                '        <option value="WI">Wisconsin</option>' +
                '        <option value="WY">Wyoming</option>' +
                '    </select>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-zip-1">' + curatedClientRole + ' Zip <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-zip-1" name="inquiry-form__other-client-zip-1" />' +
                '    <small>Please enter 5 digits</small>' +
                '</fieldset>'
            );
        } else if (this.value === 'business-entity') {
            $stepsContainer.find('#inquiry-form__step-3 .custom-form__fields-section .custom-form__fields').append(
                '<fieldset class="custom-form__field">' +
                '      <label for="inquiry-form__other-business-entity-name-1">Business Entity Name <span>*</span></label>' +
                '      <input type="text" id="inquiry-form__other-business-entity-name-1" name="inquiry-form__other-business-entity-name-1" />' +
                '      <small>If the client is an estate please enter "Estate of [Decedent’s Name]" (e.g. Estate of John Smith).</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-first-name-1">Representative First and Middle Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-first-name-1" name="inquiry-form__other-representative-first-name-1" />' +
                '    <small>If the client is an estate, please include the Executor as the Representative.</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-last-name-1">Representative Last Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-last-name-1" name="inquiry-form__other-representative-last-name-1" />' +
                '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-representative-email-1">Representative Email <span>*</span></label>' +
                // '    <input type="email" id="inquiry-form__other-representative-email-1" name="inquiry-form__other-representative-email-1" />' +
                // '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-representative-phone-1">Representative Phone <span>*</span></label>' +
                // '    <input type="text" id="inquiry-form__other-representative-phone-1" name="inquiry-form__other-representative-phone-1" />' +
                // '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-street-address-1">Representative Street Address <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-street-address-1" name="inquiry-form__other-representative-street-address-1" />' +
                '    <small>Please include apartment/unit/suite number</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-city-1">Representative City <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-city-1" name="inquiry-form__other-representative-city-1" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-state-1">Representative State <span>*</span></label>' +
                '    <select id="inquiry-form__other-representative-state-1" name="inquiry-form__other-representative-state-1">' +
                '          <option value="">Select State</option>' +
                '          <option value="AL">Alabama</option>' +
                '          <option value="AK">Alaska</option>' +
                '          <option value="AZ">Arizona</option>' +
                '          <option value="AR">Arkansas</option>' +
                '          <option value="CA">California</option>' +
                '          <option value="CO">Colorado</option>' +
                '          <option value="CT">Connecticut</option>' +
                '          <option value="DE">Delaware</option>' +
                '          <option value="DC">District Of Columbia</option>' +
                '          <option value="FL">Florida</option>' +
                '          <option value="GA">Georgia</option>' +
                '          <option value="HI">Hawaii</option>' +
                '          <option value="ID">Idaho</option>' +
                '          <option value="IL">Illinois</option>' +
                '          <option value="IN">Indiana</option>' +
                '          <option value="IA">Iowa</option>' +
                '          <option value="KS">Kansas</option>' +
                '          <option value="KY">Kentucky</option>' +
                '          <option value="LA">Louisiana</option>' +
                '          <option value="ME">Maine</option>' +
                '          <option value="MD">Maryland</option>' +
                '          <option value="MA">Massachusetts</option>' +
                '          <option value="MI">Michigan</option>' +
                '          <option value="MN">Minnesota</option>' +
                '          <option value="MS">Mississippi</option>' +
                '          <option value="MO">Missouri</option>' +
                '          <option value="MT">Montana</option>' +
                '          <option value="NE">Nebraska</option>' +
                '          <option value="NV">Nevada</option>' +
                '          <option value="NH">New Hampshire</option>' +
                '          <option value="NJ">New Jersey</option>' +
                '          <option value="NM">New Mexico</option>' +
                '          <option value="NY">New York</option>' +
                '          <option value="NC">North Carolina</option>' +
                '          <option value="ND">North Dakota</option>' +
                '          <option value="OH">Ohio</option>' +
                '          <option value="OK">Oklahoma</option>' +
                '          <option value="OR">Oregon</option>' +
                '          <option value="PA">Pennsylvania</option>' +
                '          <option value="RI">Rhode Island</option>' +
                '          <option value="SC">South Carolina</option>' +
                '          <option value="SD">South Dakota</option>' +
                '          <option value="TN">Tennessee</option>' +
                '          <option value="TX">Texas</option>' +
                '          <option value="UT">Utah</option>' +
                '          <option value="VT">Vermont</option>' +
                '          <option value="VA">Virginia</option>' +
                '          <option value="WA">Washington</option>' +
                '          <option value="WV">West Virginia</option>' +
                '          <option value="WI">Wisconsin</option>' +
                '          <option value="WY">Wyoming</option>' +
                '    </select>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-zip-1">Representative Zip <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-zip-1" name="inquiry-form__other-representative-zip-1" />' +
                '    <small>Please enter 5 digits</small>' +
                '</fieldset>'
            );
        }
    });

    $(document).on('change', '#inquiry-form__other-client-type-2, #inquiry-form__other-client-type-3, #inquiry-form__other-client-type-4, #inquiry-form__other-client-type-5, #inquiry-form__other-client-type-6, #inquiry-form__other-client-type-7, #inquiry-form__other-client-type-8, #inquiry-form__other-client-type-9, #inquiry-form__other-client-type-10', function () {
        var clientRole = $inquiryForm.find('#inquiry-form__client-role-1 option:selected').val();
        var curatedClientRole = clientRole === 'buyer' ? 'Seller' : 'Buyer';
        if ($(this).parent('.custom-form__field').siblings().length > 1) {
            var $fields = $(this).parents('.custom-form__fields').find('.custom-form__field:nth-child(n+2)');
            if ($fields.length) {
                $.each($fields, function(index, value) {
                    $(value).remove();
                });
            }
        }

        if ($inquiryForm.find('#inquiry-form__other-client-type-1 option:selected').val() === this.value && $inquiryForm.find('.custom-form__other-copy').hasClass('custom-form__other-copy--inactive')) {
            $inquiryForm.find('.custom-form__other-copy').removeClass('custom-form__other-copy--inactive');
        }

        if (this.value === 'individual') {
            $stepsContainer.find('#inquiry-form__step-3 .custom-form__fields-section:last-child  .custom-form__fields').append(
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-first-name-' + otherIndividualCounter + '">' + curatedClientRole + ' First and Middle Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-first-name-' + otherIndividualCounter + '" name="inquiry-form__other-client-first-name-' + otherIndividualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-last-name-' + otherIndividualCounter + '">' + curatedClientRole + ' Last Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-last-name-' + otherIndividualCounter + '" name="inquiry-form__other-client-last-name-' + otherIndividualCounter + '" />' +
                '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-client-email-' + otherIndividualCounter + '">' + curatedClientRole + ' Email <span>*</span></label>' +
                // '    <input type="email" id="inquiry-form__other-client-email-' + otherIndividualCounter + '" name="inquiry-form__other-client-email-' + otherIndividualCounter + '" />' +
                // '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-client-phone-' + otherIndividualCounter + '">' + curatedClientRole + ' Phone <span>*</span></label>' +
                // '    <input type="text" id="inquiry-form__other-client-phone-' + otherIndividualCounter + '" name="inquiry-form__other-client-phone-' + otherIndividualCounter + '" />' +
                // '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-street-address-' + otherIndividualCounter + '">' + curatedClientRole + ' Street Address <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-street-address-' + otherIndividualCounter + '" name="inquiry-form__other-client-street-address-' + otherIndividualCounter + '" />' +
                '    <small>Please include apartment/unit/suite number</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-city-' + otherIndividualCounter + '">' + curatedClientRole + ' City <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-city-' + otherIndividualCounter + '" name="inquiry-form__other-client-city-' + otherIndividualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-state-' + otherIndividualCounter + '">' + curatedClientRole + ' State <span>*</span></label>' +
                '    <select id="inquiry-form__other-client-state-' + otherIndividualCounter + '" name="inquiry-form__other-client-state-' + otherIndividualCounter + '">' +
                '        <option value="">Select State</option>' +
                '        <option value="AL">Alabama</option>' +
                '        <option value="AK">Alaska</option>' +
                '        <option value="AZ">Arizona</option>' +
                '        <option value="AR">Arkansas</option>' +
                '        <option value="CA">California</option>' +
                '        <option value="CO">Colorado</option>' +
                '        <option value="CT">Connecticut</option>' +
                '        <option value="DE">Delaware</option>' +
                '        <option value="DC">District Of Columbia</option>' +
                '        <option value="FL">Florida</option>' +
                '        <option value="GA">Georgia</option>' +
                '        <option value="HI">Hawaii</option>' +
                '        <option value="ID">Idaho</option>' +
                '        <option value="IL">Illinois</option>' +
                '        <option value="IN">Indiana</option>' +
                '        <option value="IA">Iowa</option>' +
                '        <option value="KS">Kansas</option>' +
                '        <option value="KY">Kentucky</option>' +
                '        <option value="LA">Louisiana</option>' +
                '        <option value="ME">Maine</option>' +
                '        <option value="MD">Maryland</option>' +
                '        <option value="MA">Massachusetts</option>' +
                '        <option value="MI">Michigan</option>' +
                '        <option value="MN">Minnesota</option>' +
                '        <option value="MS">Mississippi</option>' +
                '        <option value="MO">Missouri</option>' +
                '        <option value="MT">Montana</option>' +
                '        <option value="NE">Nebraska</option>' +
                '        <option value="NV">Nevada</option>' +
                '        <option value="NH">New Hampshire</option>' +
                '        <option value="NJ">New Jersey</option>' +
                '        <option value="NM">New Mexico</option>' +
                '        <option value="NY">New York</option>' +
                '        <option value="NC">North Carolina</option>' +
                '        <option value="ND">North Dakota</option>' +
                '        <option value="OH">Ohio</option>' +
                '        <option value="OK">Oklahoma</option>' +
                '        <option value="OR">Oregon</option>' +
                '        <option value="PA">Pennsylvania</option>' +
                '        <option value="RI">Rhode Island</option>' +
                '        <option value="SC">South Carolina</option>' +
                '        <option value="SD">South Dakota</option>' +
                '        <option value="TN">Tennessee</option>' +
                '        <option value="TX">Texas</option>' +
                '        <option value="UT">Utah</option>' +
                '        <option value="VT">Vermont</option>' +
                '        <option value="VA">Virginia</option>' +
                '        <option value="WA">Washington</option>' +
                '        <option value="WV">West Virginia</option>' +
                '        <option value="WI">Wisconsin</option>' +
                '        <option value="WY">Wyoming</option>' +
                '    </select>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-client-zip-' + otherIndividualCounter + '">' + curatedClientRole + ' Zip <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-client-zip-' + otherIndividualCounter + '" name="inquiry-form__other-client-zip-' + otherIndividualCounter + '" />' +
                '    <small>Please enter 5 digits</small>' +
                '</fieldset>'
            );
        } else if (this.value === 'business-entity') {
            $stepsContainer.find('#inquiry-form__step-3 .custom-form__fields-section:last-child .custom-form__fields').append(
                '<fieldset class="custom-form__field">' +
                '      <label for="inquiry-form__other-business-entity-name-' + otherIndividualCounter + '">Business Entity Name <span>*</span></label>' +
                '      <input type="text" id="inquiry-form__other-business-entity-name-' + otherIndividualCounter + '" name="inquiry-form__other-business-entity-name-' + otherIndividualCounter + '" />' +
                '      <small>If the client is an estate please enter "Estate of [Decedent’s Name]" (e.g. Estate of John Smith).</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-first-name-' + otherIndividualCounter + '">Representative First and Middle Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-first-name-' + otherIndividualCounter + '" name="inquiry-form__other-representative-first-name-' + otherIndividualCounter + '" />' +
                '    <small>If the client is an estate, please include the Executor as the Representative.</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-last-name-' + otherIndividualCounter + '">Representative Last Name <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-last-name-' + otherIndividualCounter + '" name="inquiry-form__other-representative-last-name-' + otherIndividualCounter + '" />' +
                '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-representative-email-' + otherIndividualCounter + '">Representative Email <span>*</span></label>' +
                // '    <input type="email" id="inquiry-form__other-representative-email-' + otherIndividualCounter + '" name="inquiry-form__other-representative-email-' + otherIndividualCounter + '" />' +
                // '</fieldset>' +
                // '<fieldset class="custom-form__field">' +
                // '    <label for="inquiry-form__other-representative-phone-' + otherIndividualCounter + '">Representative Phone <span>*</span></label>' +
                // '    <input type="text" id="inquiry-form__other-representative-phone-' + otherIndividualCounter + '" name="inquiry-form__other-representative-phone-' + otherIndividualCounter + '" />' +
                // '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-street-address-' + otherIndividualCounter + '">Representative Street Address <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-street-address-' + otherIndividualCounter + '" name="inquiry-form__other-representative-street-address-' + otherIndividualCounter + '" />' +
                '    <small>Please include apartment/unit/suite number</small>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-city-' + otherIndividualCounter + '">Representative City <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-city-' + otherIndividualCounter + '" name="inquiry-form__other-representative-city-' + otherIndividualCounter + '" />' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-state-' + otherIndividualCounter + '">Representative State <span>*</span></label>' +
                '    <select id="inquiry-form__other-representative-state-' + otherIndividualCounter + '" name="inquiry-form__other-representative-state-' + otherIndividualCounter + '">' +
                '          <option value="">Select State</option>' +
                '          <option value="AL">Alabama</option>' +
                '          <option value="AK">Alaska</option>' +
                '          <option value="AZ">Arizona</option>' +
                '          <option value="AR">Arkansas</option>' +
                '          <option value="CA">California</option>' +
                '          <option value="CO">Colorado</option>' +
                '          <option value="CT">Connecticut</option>' +
                '          <option value="DE">Delaware</option>' +
                '          <option value="DC">District Of Columbia</option>' +
                '          <option value="FL">Florida</option>' +
                '          <option value="GA">Georgia</option>' +
                '          <option value="HI">Hawaii</option>' +
                '          <option value="ID">Idaho</option>' +
                '          <option value="IL">Illinois</option>' +
                '          <option value="IN">Indiana</option>' +
                '          <option value="IA">Iowa</option>' +
                '          <option value="KS">Kansas</option>' +
                '          <option value="KY">Kentucky</option>' +
                '          <option value="LA">Louisiana</option>' +
                '          <option value="ME">Maine</option>' +
                '          <option value="MD">Maryland</option>' +
                '          <option value="MA">Massachusetts</option>' +
                '          <option value="MI">Michigan</option>' +
                '          <option value="MN">Minnesota</option>' +
                '          <option value="MS">Mississippi</option>' +
                '          <option value="MO">Missouri</option>' +
                '          <option value="MT">Montana</option>' +
                '          <option value="NE">Nebraska</option>' +
                '          <option value="NV">Nevada</option>' +
                '          <option value="NH">New Hampshire</option>' +
                '          <option value="NJ">New Jersey</option>' +
                '          <option value="NM">New Mexico</option>' +
                '          <option value="NY">New York</option>' +
                '          <option value="NC">North Carolina</option>' +
                '          <option value="ND">North Dakota</option>' +
                '          <option value="OH">Ohio</option>' +
                '          <option value="OK">Oklahoma</option>' +
                '          <option value="OR">Oregon</option>' +
                '          <option value="PA">Pennsylvania</option>' +
                '          <option value="RI">Rhode Island</option>' +
                '          <option value="SC">South Carolina</option>' +
                '          <option value="SD">South Dakota</option>' +
                '          <option value="TN">Tennessee</option>' +
                '          <option value="TX">Texas</option>' +
                '          <option value="UT">Utah</option>' +
                '          <option value="VT">Vermont</option>' +
                '          <option value="VA">Virginia</option>' +
                '          <option value="WA">Washington</option>' +
                '          <option value="WV">West Virginia</option>' +
                '          <option value="WI">Wisconsin</option>' +
                '          <option value="WY">Wyoming</option>' +
                '    </select>' +
                '</fieldset>' +
                '<fieldset class="custom-form__field">' +
                '    <label for="inquiry-form__other-representative-zip-' + otherIndividualCounter + '">Representative Zip <span>*</span></label>' +
                '    <input type="text" id="inquiry-form__other-representative-zip-' + otherIndividualCounter + '" name="inquiry-form__other-representative-zip-' + otherIndividualCounter + '" />' +
                '    <small>Please enter 5 digits</small>' +
                '</fieldset>'
            );
        }
    });

    $(document).on('click', '.custom-form__other-remove', function(e) {
        e.preventDefault();
        var $section = $(this).parents('.custom-form__fields-section');

        if ($section.length) {
            $section.remove();
            otherIndividualCounter--;
        }
    });
});
